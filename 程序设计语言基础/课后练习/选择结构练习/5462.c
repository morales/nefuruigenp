#include<stdio.h>
int main()
{
	int y, m, d;
	scanf("%d%d", &y, &m);
	if(m==2) d=28+(y%4==0&&y%100||y%400==0);
	else if(y==4||y==6||y==9||y==11) d=30;
	else d=31;
	printf("%d\n", d);
	return 0;
}
