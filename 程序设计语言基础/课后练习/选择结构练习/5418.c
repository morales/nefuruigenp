#include<stdio.h>
int main()
{
	double inp, cnt=0.0;
	scanf("%lf", &inp);
	if(inp <= 50)
		cnt = inp*0.53;
	else if(inp>100)
		cnt = 50*0.53+50*0.58+(inp-100)*0.65;
	else cnt = 50*0.53+(inp-50)*0.58;
	printf("%.2lf\n", cnt);
	return 0;
}
