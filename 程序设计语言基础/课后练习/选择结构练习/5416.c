#include<stdio.h>
double n, m;
char ch;
int main()
{
	scanf("%lf %c%lf", &n, &ch, &m);
	switch(ch)
	{
		case '+':
			printf("%.2lf\n", n+m);
			break;
		case '-':
			printf("%.2lf\n", n-m);
			break;
		case '*':
			printf("%.2lf\n", n*m);
			break;
		case '/':
			printf("%.2lf\n", n/m);
			break;
	}
	return 0;
}
