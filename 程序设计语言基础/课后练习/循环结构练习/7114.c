#include<stdio.h>
int main()
{
	char ch;
	int i, a[27];
	for(i=0; i<=26; i++)
		a[i] = 0;
	while(scanf("%c", &ch) != EOF)
	{
		if(ch>='A' && ch<='Z')
			a[ch-65]++;
	}
	for(i=0; i<26; i++)
		printf("%c %d\n", i+65, a[i]);
	return 0;
}
