#include<stdio.h>
int a[10];
int main()
{
	while(scanf("%d", &a[0]) != EOF)
	{
		int maxn=-9999, flag1=-1;
		int minn=1e9, flag2=-1;
		if(a[0] > maxn)
		{
			maxn = a[0];
			flag1 = 0;
		}
		if(a[0] < minn)
		{
			minn = a[0];
			flag2 = 0;
		}
		for(int i=1; i<10; i++)
		{
			scanf("%d", &a[i]);
			if(a[i] > maxn)
			{
				maxn = a[i];
				flag1 = i;
			}
			if(a[i] < minn)
			{
				minn = a[i];
				flag2 = i;
			}
		}
		int tmp = a[flag1];
		a[flag1] = a[flag2];
		a[flag2] = tmp;
		for(int i=0; i<9; i++)
			printf("%d ", a[i]);
		printf("%d\n", a[9]);
	}
	return 0;
}
