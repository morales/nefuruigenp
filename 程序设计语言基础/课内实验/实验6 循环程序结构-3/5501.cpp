#include<stdio.h>
int n;
int main()
{
    while(scanf("%d", &n) != EOF)
	{
		if(n == 0)
			printf("1.000000\n");
		else
		{
			double e = 1.0;
			for(int i=1; ; i++)
			{
				double tmp=1.0, tmp2=1.0;
				for(int j=1; j<=i; j++)
					tmp *= j;
				for(int j=1; j<=i; j++)
					tmp2 *= n;
				e += tmp2*1.0/tmp;
				if((tmp2*1.0/tmp) < 0.00001)
				{
					printf("%.6lf\n", e);
					break;
				}
			}
		}
	}
	return 0;
}
