#include<stdio.h>
int n;
int main()
{
	while(scanf("%d", &n) != EOF)
	{
		double e = 1.0;
		for(int i=1; i<n; i++)
		{
			int tmp = 1;
			for(int j=1; j<=i; j++)
				tmp *= j;
			e += 1.0/tmp;
		}
		printf("%.4lf\n", e);
	}
	return 0;
}
