#include<stdio.h>
#include<math.h>
int n;
int main()
{
	while(scanf("%d", &n) != EOF)
	{
		double sinx=n*1.0;
		if(n == 0)
			printf("0.000000\n");
		else
		{
			for(int i=2; ; i++)
			{
				double cheng=1.0, jc=1.0;
				for(int j=1; j<=i*2-1; j++)
					cheng *= n;
				for(int j=1; j<=i*2-1; j++)
					jc *= j;
				if(cheng*1.0/jc <= 0.00001)
					break;	
				if(i%2) sinx += cheng*1.0/(jc*1.0);
				else sinx -= cheng*1.0/(jc*1.0);
			}
			printf("%.6lf\n", sinx);
		}
	}
	return 0;
}
