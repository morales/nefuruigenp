#include<stdio.h>
int n;
int main()
{
	while(scanf("%d", &n) != EOF)
	{
		double pi = 4.0;
		int s = -1;
		for(int i=1; i<n; i++)
		{
			pi += s*4.0/(i*2+1);
			s = -s;
		}
		printf("%.10lf\n", pi);
	}
	return 0;
}
