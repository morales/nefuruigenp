#include<stdio.h>
double a[7];
int main()
{
	while(scanf("%lf", &a[0]) != EOF)
	{
		double maxn=-9999.0, minn=9999.0, cnt=0;
		if(a[0] > maxn)
			maxn = a[0];
		if(a[0] < minn)
			minn = a[0];
		for(int i=1; i<7; i++)
		{
			scanf("%lf", &a[i]);
			if(a[i] > maxn)
				maxn = a[i];
			if(a[i] < minn)
				minn = a[i];
		}
		for(int i=0; i<7; i++)
			cnt += a[i];
		double ans = (cnt-maxn-minn)/5;
		printf("%.2lf\n", ans);
	}
	return 0;
}
