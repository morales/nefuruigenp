#include<stdio.h>
#include<string.h>
char a[100], b[100];
int main()
{
	while(scanf("%s%s", a, b) != EOF)
	{
		int n = strlen(a);
		int m = strlen(b);
		int cnt1=0, cnt2=0;
		for(int i=0; i<n; i++)
			cnt1 += a[i];
		for(int i=0; i<m; i++)
			cnt2 += b[i];
		if(cnt1 > cnt2)
			printf("1\n");
		else if(cnt1 < cnt2)
			printf("-1\n");
		else printf("0\n");
	}
	return 0;
}
