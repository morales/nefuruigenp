#include <stdio.h>
#include <stdlib.h>
int main()
{
    int n;
    long long  func(int n);
    while(scanf("%d",&n)!=EOF)
    {
      printf("%I64d\n",func(n));
    }
    return 0;
}
//start 
long long func(int n)
{
	long long tmp = 1;
	for(int i=2; i<=n; i++)
		tmp *= i;
	return tmp;
}
//end
