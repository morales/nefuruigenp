#include <stdio.h>
 #include <stdlib.h>
int main()
{
    int x,y;
    long z;
    long IntPower(int base,int exp);
    while(scanf("%d%d",&x,&y)!=EOF)
    {
        z=IntPower(x,y);
        printf("%ld\n",z);
    }
    return 0;
}
 //start
long IntPower(int base, int exp)
{
	int tmp = base;
	for(int i=1; i<exp; i++)
		base *= tmp;
	return base;
} 
 //end
