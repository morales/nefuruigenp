#include<stdio.h>
double x, y, ans;
int main()
{
    scanf("%lf%lf", &x, &y);
	double n = x-y-3500;
	if(n <= 1500)
		ans = n*0.03;
	else if(n>1500 && n<=4500)
		ans = n*0.1 - 105;
	else if(n>4500 && n<=9000)
		ans = n*0.2 - 555;
	else if(n>9000 && n<=35000)
		ans = n*0.25 - 1005;
	else if(n>35000 && n<=55000)
		ans = n*0.3 - 2775;
	else if(n>55000 && n<=80000)
		ans = n*0.35 - 5505;
	else ans = n*0.45 - 13505;
	printf("%.2lf", ans);
	return 0;
} 
