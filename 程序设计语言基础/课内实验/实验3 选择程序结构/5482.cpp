#include<stdio.h>
#include<math.h>
double a, b, c, s, s1, s2;
int main()
{
    scanf("%lf%lf%lf", &a, &b, &c);
    s = b*b - 4*a*c;
    s1 = (-1.0*b+sqrt(s))/(a*2);
    s2 = (-1.0*b-sqrt(s))/(a*2);
    printf("%.2lf %.2lf", s1, s2);
    return 0;
}
