#include<stdio.h>
int a[10];
void quicksort(int a[], int L, int R)
{
    int i=L,j=R;
	int mid = a[(L+R)/2];
	while(i <= j)
	{
		while(a[i] < mid)
			i++;
		while(a[j] > mid)
			j--;
		if(i <= j)
		{
			int tmp = a[i];
			a[i] = a[j];
			a[j] = tmp;
			i++;
			j--;
		}
	}
	if(j > L)
		quicksort(a, L, j);
	if(i < R)
		quicksort(a, i, R);
}
int main()
{
	while(scanf("%d", &a[0]) != EOF)
	{
		for(int i=1; i<10; i++)
			scanf("%d", &a[i]);
		quicksort(a, 0, 9);
		for(int i=9; i>0; i--)
			printf("%d ", a[i]);
		printf("%d\n", a[0]);
	}
	return 0;
}
