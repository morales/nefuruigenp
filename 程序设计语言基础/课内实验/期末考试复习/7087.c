#include<stdio.h>
double x, y;
char ch;
int main()
{
    scanf("%lf%c%lf", &x, &ch, &y);
	switch(ch)
	{
		case '+':
			printf("%.2lf", x+y);
			break;
		case '-':
			printf("%.2lf", x-y);
			break;
		case '*':
			printf("%.2lf", x*y);
			break;
		case '/':
			printf("%.2lf", x/y);
			break;
	}
	return 0;
}
