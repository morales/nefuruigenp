#include<stdio.h>
int n, cnt=0;
double s, d, ans=0;
int main()
{
    scanf("%d%lf", &n, &s);
	for(int i=1; i<=n; i++)
	{
		scanf("%lf", &d);
		if(d>=s) 
			cnt++;
		ans += d;
	}
	printf("%d %.2lf", cnt, ans*1.0/n);
	return 0;
}
