#include <stdio.h>
#include <stdlib.h>
int main()
{
    int a[20],b[20],i,j,n,t;
    while(scanf("%d",&n)>0)
    {
        for(i=0;i<n;i++)
            scanf("%d",&a[i]);
        for(i=0;i<n-1;i++)
            for(j=0;j<n-1-i;j++)
                if(a[j]>a[j+1])
                {
                    t = a[j];
                    a[j] = a[j+1];
                    a[j+1] = t;
                }
        //start
        int head=0, tail=n, tot=0;
    	while(head < tail)
		{
			printf("%d ", a[head]);
			head++;
			b[tot++] = a[head];
			head++;
		}
		if(n%2)
			for(i=tot-2; i>=0; i--)
				printf("%d ", b[i]);
		else
			for(i=tot-1; i>=0; i--)
				printf("%d ", b[i]);
		printf("\n");
        //end
    }
    return 0;
}
