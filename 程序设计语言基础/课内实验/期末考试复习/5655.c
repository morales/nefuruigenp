#include<stdio.h>
#include<math.h>
int judge(int n)
{
    int k=2;
	if(n==2) return 1;
	while(k<=sqrt(n) && n%k!=0) k++;
	if(n%k==0) return 0;
	else return 1;
}
int main()
{
	int n, cnt=0;
	scanf("%d", &n);
	for(int i=2; i<=n; i++)
		if(judge(i))
			cnt++;
	printf("%d", cnt);
	return 0;
}
