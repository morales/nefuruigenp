#include<stdio.h>
double num[12];
int swap(int x, int y)
{
	int tmp = x;
	x = y;
	y = tmp;
}
void quicksort(double a[], int L, int R)
{
	int i=L,j=R;
	int mid=a[(L+R)/2];
	while(i<=j)
	{
		while(a[i]<mid)
			i++;
		while(a[j]>mid)
			j--;
		if(i<=j)
		{
			swap(a[i],a[j]);
			i++;
			j--;
		}
	}
	if(j>L)
		quicksort(a, L, j);
	if(i<R)
		quicksort(a, i, R);
}
int main()
{
	for(int i=1; i<=10; i++)
		scanf("%lf", &num[i]);
	quicksort(num, 1, 10);
	for(int i=10; i>=1; i--)
		printf("%.1lf ", num[i]);
	return 0;
}
