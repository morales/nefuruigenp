#include<stdio.h>
#include<math.h>
int n, a, b, c, ans;
int ju(int m)
{
    for(int j=2; j<=sqrt(m); j++)
        if(m%j == 0)
            return 0;
    return 1;
}
int main()
{
    while(scanf("%d", &n) != EOF)
    {
        ans = 0;
        for(int i=2; i<=n; i++)
            if(ju(i))
                ans += i;
        printf("%d\n", ans);
    }
    return 0;
}
