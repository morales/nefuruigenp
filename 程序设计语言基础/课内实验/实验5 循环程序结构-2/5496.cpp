#include<stdio.h>
#include<math.h>
int n, a, b, c, ans;
int main()
{
    while(scanf("%d", &n) != EOF)
    {
        ans = 1;
        for(int i=2; i<=n; i++)
            ans *= i;
        printf("%d\n", ans);
    }
    return 0;
}
