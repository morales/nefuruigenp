#include<stdio.h>
int n, t=0, flag=0, a[1050];
int judge(int m)
{
	int x=m/100, y=(m/10)%10, z=m%10;
	if(x%2==0 && y%2==0 && z%2==0) return 1;
	else return 0;
}
int main()
{
	scanf("%d", &n);
	for(int i=100; i<=n; i++)
		if(judge(i))
			a[++t] = i;
	for(int i=1; i<=t; i++)
	{
		if(flag>0 && flag%10==0)
			printf("\n");
		flag++;
		printf("%d ", a[i]);
	}
	return 0;
}
