#include<stdio.h>
int n;
double pi=0;
int main()
{
	scanf("%d", &n);
	for(int i=1; i<=n; i++)
	{
		if(i%2)
			pi += 4.0/(i*2-1);
		else pi -= 4.0/(i*2-1);
	}
	printf("%.10lf", pi);
	return 0;
}
