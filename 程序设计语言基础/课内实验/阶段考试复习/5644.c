#include<stdio.h>
#include<math.h>
int n, m, ans=1, pd=0;
int judge(int x)
{
	if(x==1) return 0;
	else
	{
		int k=2;
		while(k<=sqrt(x) && x%k!=0)
			k++;
		if(k > sqrt(x)) return 1;
		else return 0;
	}
}
int main()
{
	while(scanf("%d", &n) != EOF)
	{
		ans = 1;
		pd = 0;
		for(int i=1; i<=n; i++)
		{
			scanf("%d", &m);
			if(judge(m))
			{
				ans *= m;
				pd++;
			}
		}
		if(!pd) ans -= 1;
		printf("%d\n", ans);		
	}
	return 0;
}
