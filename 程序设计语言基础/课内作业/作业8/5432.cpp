#include <stdio.h>
#include <stdlib.h>
int main()
{
    int least_common_multiple(int x, int y);
    int x, y, z;
    scanf("%d%d%d",&x,&y,&z);
    printf("%d\n",least_common_multiple(least_common_multiple(x,y),z));
    return 0;
}
int least_common_multiple(int x, int y)
{
    //start
    int r=1, m=x, n=y;
    if(x < y)
    {
    	int tmp = x;
    	x = y;
    	y = tmp;
	}
	while(r != 0)
	{
		r = x%y;
		x = y;
		y = r;
	}
	return m*n/x;
    //end
}
