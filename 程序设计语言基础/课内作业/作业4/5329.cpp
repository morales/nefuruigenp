#include <stdio.h>
#include <stdlib.h>
int main()
{
   int num, digit1, digit2, digit3, digit4, digit5;
   digit1 = digit2 = digit3 = digit4 = digit5 = 0;
   printf("Enter a number between 0 and 32767: ");
   scanf("%d",&num);
   //start
    int a[5]={0}, i=0;
	while(num)
	{
		a[i] = num%8;
		i++;
		num /= 8;
	}
	digit5 = a[0];
	digit4 = a[1];
	digit3 = a[2];
	digit2 = a[3];
	digit1 = a[4];
   //end
   printf("In octal, your number is: %d%d%d%d%d\n",digit1,digit2,digit3,digit4,digit5);
   return 0;
}
