/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-16 08:52:36
 * @LastEditTime: 2020-10-16 08:57:00
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<stdlib.h>

typedef struct QNode
{
    int data;
	struct QNode *next;
}QNode,*QueuePtr;

typedef struct{
	QueuePtr front;
	QueuePtr rear;
}LinkQueue;


int InitQueue(LinkQueue &Q)
{
	Q.front = Q.rear = (QueuePtr)malloc(sizeof(QNode));
	if( !Q.front) exit(0);
	Q.front->next = NULL;
	return 1;
}

int EnQueue(LinkQueue &Q, int e)
{
	QNode *p = (QueuePtr)malloc(sizeof(QNode));
    p->data = e;
    p->next = NULL;
    Q.rear->next = p;
    Q.rear = p;
    return 1;
}

int DeQueue(LinkQueue &Q,int &e)
{
    if(Q.front == Q.rear) return 0;
    QNode *p = Q.front->next;
    e = p->data;
    Q.front->next = p->next;
    if(Q.rear == p) Q.rear = Q.front;
    free(p);
    return 1;
}
int main()
{
	int m,n;
	int i,e;
	LinkQueue Q;
	InitQueue(Q);
	scanf("%d",&n);
	for(i = 0 ; i < n ; ++i)
	{
		scanf("%d",&e);
		EnQueue(Q,e);
	}
	scanf("%d",&m);
	for(i = 0 ; i < m ; ++i)
	{
		DeQueue(Q,e);
		printf("%d ",e);
	}
	printf("\n");
	return 0;
}
