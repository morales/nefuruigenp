/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-13 19:57:45
 * @LastEditTime: 2020-10-13 19:58:40
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
using namespace std;

int sum(int a[],int n)
{
    //write your code here
    if(n==1) return a[n-1];
    else return a[n-1]+sum(a, n-1);
}

int main()
{
	int a[6],i;
	for (i=0;i<6;i++)
		cin >> a[i];
	cout << sum(a,6) << endl;
	return 0;
}