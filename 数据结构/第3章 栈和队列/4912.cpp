/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-13 19:59:44
 * @LastEditTime: 2020-10-13 20:00:26
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
using namespace std;

int max(int a[],int n)
{
    //write your code here
    int maxn=-0x3f3f3f3f;
    for(int i=0; i<n; i++)
        if(maxn < a[i]) maxn = a[i];
    return maxn;
}

int main()
{
	int a[6],i;
	for (i=0;i<6;i++)
		cin >> a[i];
	cout << max(a,6) << endl;
	return 0;
}