/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-13 20:20:26
 * @LastEditTime: 2020-10-13 20:20:28
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>

#define maxsize 10000

typedef struct 
{
    int data[maxsize];      //存放栈中元素
	int top;                //栈顶指针
}SqStack;

//初始化栈
void initStack(SqStack &st)
{
	st.top = -1;
}
bool isEmpty(SqStack st)
{
    if(st.top == -1) return 1;
    return 0;
}
//进栈算法
int Push(SqStack &st, int x)
{
	if(st.top == maxsize - 1)
		return 0;
	++(st.top);  //先移动指针再进栈
	st.data[st.top] = x;
	return 1;
}
bool print_S(SqStack st)
{
    if( isEmpty(st) )
	{
		printf("Stack is Empty");
		return 0;
	}
	int iPointer = st.top;
	while(iPointer != -1)
	{
		printf("%d ",st.data[iPointer]);
		--iPointer;
	}
	printf("\n");
	return 1;
}


int main()
{
	int n , i , e;
	SqStack S;
	initStack(S);
	scanf("%d",&n);
	for(i = 0 ; i < n; ++i)
	{
		scanf("%d",&e);
		Push(S,e);
	}
	print_S(S);    //需要写出的函数：打印栈
	return 0;
}