/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-13 19:33:48
 * @LastEditTime: 2020-10-13 20:02:01
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
#include<cstdio>
#include<iomanip>
using namespace std;

//write your code here
float sum(float a[], int i)
{
    //cout<<a[i]<<endl;
    if(i==0) return a[i];
    else return (a[i]+sum(a, i-1));
}

int main()
{
	float a[6];
	int i;
	for (i=0;i<6;i++)
		cin >> a[i];
    /*for (i=0;i<6;i++)
		cout<<a[i]<<" ";
	//write your code here
    //cout<<sum(a, 5)<<endl;*/
    //cout<<setprecision(1)<<sum(a, 5)/6.0<<endl;
    printf("%g", sum(a, 5)/6);
	return 0;
}