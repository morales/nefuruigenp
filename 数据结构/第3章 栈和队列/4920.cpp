/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-13 18:19:04
 * @LastEditTime: 2020-10-13 19:32:26
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
using namespace std;

class SeqQueue {
private:
    int *q;
	int maxSize;
    int front;
	int rear;
	int length;
    bool flag;
public:
	SeqQueue(int sz=0);
	void EnQueue(const int &x);
	bool DeQueue(int &x);
	bool isEmpty() const;
	bool isFull() const;
	void output()
	{
		int i;
		for (i=0;i<maxSize;i++)
			cout << q[i] << " ";
	}
};

SeqQueue::SeqQueue(int sz):rear(0),length(0),front(0)
{
	maxSize = sz;
	q = new int[maxSize];
	int i;
	for (i=0;i<maxSize;i++)
		q[i] = 0;
}

//write your code here
void SeqQueue::EnQueue(const int &x)
{
    /*cout<<"rear="<<rear<<endl;
    cout<<"front="<<front<<endl;*/
    if(length == maxSize) return;
    q[rear] = x;
    rear = (rear+1)%maxSize;
    length++;
    /*output();
    cout<<"g"<<endl;
    cout<<endl;*/
}
bool SeqQueue::DeQueue(int &x)
{
    //cout<<"defront="<<front<<endl;
    if(!length) return false;
    x = q[front];
    q[front] = 0;
    //cout<<"x"<<x<<endl;
    front = (front+1)%maxSize;
    length--;
    /*cout<<"x "<<q[front]<<endl;
    output();
    cout<<"j"<<endl;*/
    return true;
}
bool SeqQueue::isEmpty() const
{
    if(front == rear) return true;
    return false;
}
bool SeqQueue::isFull() const
{
    if((rear+1)%maxSize == front) return true;
    return false;
}

int main()
{
	SeqQueue sq(4);
	int i;
	sq.EnQueue(1);
	sq.EnQueue(2);
	sq.EnQueue(3);
	sq.EnQueue(4);
	sq.EnQueue(5);
	sq.DeQueue(i);
	sq.DeQueue(i);
	sq.EnQueue(6);
	sq.DeQueue(i);
	sq.output();
	return 0;
}