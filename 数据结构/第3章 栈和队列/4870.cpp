/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-13 20:23:50
 * @LastEditTime: 2020-10-13 20:23:58
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>

#define maxsize 10000

typedef struct 
{
    int data[maxsize];      //存放栈中元素
    int top;                //栈顶指针
}SqStack;

//初始化栈
void initStack(SqStack &st)
{
	st.top = -1;
}

//进栈算法
int Push(SqStack &st, int x)
{
	if(st.top == maxsize - 1)
		return 0;
	++(st.top);  //先移动指针再进栈
	st.data[st.top] = x;
	return 1;
}

//出栈算法
int Pop(SqStack &st , int &x)
{
	if(st.top == -1)
		return 0;
	x = st.data[st.top];
	--(st.top);
	return 1;
}

void isEmpty(SqStack st)
{
 //write your own codes   
    if(st.top == -1) printf("Stack is empty\n");
    else printf("Stack is not empty\n");
}

int main()
{
	int n , i , e;
	SqStack S;
	initStack(S);
	isEmpty(S);              //需要写的函数
	scanf("%d",&n);
	for(i = 0 ; i < n; ++i)
	{
		scanf("%d",&e);
		Push(S,e);
	}
	isEmpty(S);
	scanf("%d",&n);
	for(int i = 0 ;i < n ; ++i)
		Pop(S,e);
	isEmpty(S);
	return 0;
}