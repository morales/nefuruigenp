/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-13 20:06:28
 * @LastEditTime: 2020-10-13 20:18:55
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<bits/stdc++.h>
using namespace std;
#define maxsize 10000

typedef struct 
{
    int data[maxsize];      //存放栈中元素
	int top;                //栈顶指针
}SqStack;

//初始化栈
void initStack(SqStack &st)
{
	st.top = -1;
}
bool isEmpty(SqStack st)
{
    if(st.top == -1) return 1;
    return 0;
}
void Push(SqStack &st, int e)
{
    if(st.top==maxsize-1) return;
    st.data[++st.top] = e;
    //cout<<st.top<<endl;
}
void Pop(SqStack &st, int &e)
{
    if(isEmpty(st)) return;
    e = st.data[st.top--];
    //cout<<st.top<<endl;
}

//输出栈中的元素
int print_S(SqStack st)
{
	if( isEmpty(st) )
	{
		printf("Stack is Empty");
		return 0;
	}
	int iPointer = st.top;
	while(iPointer != -1)
	{
		printf("%d ",st.data[iPointer]);
		--iPointer;
	}
	printf("\n");
	return 1;
}

int main()
{
	int n , i , e;
	SqStack S;
	initStack(S);
	scanf("%d",&n);
	for(i = 0 ; i < n; ++i)
	{
		scanf("%d",&e);
		Push(S,e);                          //要写的函数：进栈操作
	}
   // cout<<S.top<<endl;
	print_S(S);
	scanf("%d",&n);
	for(int i = 0 ;i < n ; ++i)
	{
		Pop(S,e);                          //要写的函数：出栈操作
		printf("%d ",e);
	}
	printf("\n");
	print_S(S);
	return 0;
}