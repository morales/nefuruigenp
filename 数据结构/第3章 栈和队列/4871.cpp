/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-16 08:42:07
 * @LastEditTime: 2020-10-16 08:51:25
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>

#define maxsize 10000
int match(char exp[] , int n)
{
   //write your codes here
   int top = 0;
   for(int i=0; i<n; i++)
   {
       if(exp[i] == '(') top++;
       if(exp[i] == ')') top--;
   }
   if(!top) return 1;
   else return 0;
}

int main()
{
	char exp[1000];
	int n;
	scanf("%d",&n);
	scanf("%s",exp);
	if( match(exp,n) )
		printf("match\n");
	else
		printf("not match\n");
	return 0;
}