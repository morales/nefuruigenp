/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-03 16:29:03
 * @LastEditTime: 2020-10-03 16:44:50
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
using namespace std;

struct Node {
    int data;
	Node *next;
};

class List {
private:
	Node *first;
public:
	List();
	~List();
	void Insert(int x);
	bool SearchK(int k,int &val);
};

List::List()
{
	first = new Node();
	first->next = NULL;
}

List::~List()
{
}

void List::Insert(int x)
{
	Node *temp = first;
	while (temp->next!=NULL)
		temp = temp->next;
	Node *n = new Node();
	n->data = x;
	n->next = NULL;
	temp->next = n;
}

//write your code here
bool List::SearchK(int k, int &val)
{
    int cnt=0, tot=0;
    Node *p = first->next;
    while(p != NULL)
    {
        p = p->next;
        tot++;
    }
    p = first->next;
    while(p != NULL)
    {
        if(cnt == tot-k)
        {
            val = p->data;
            return 1;
        }
        p = p->next;
        cnt++;
    }
    return 0;
}

int main()
{
	List l;
	int i;
	cin >> i;
	while (i!=-1) {
		l.Insert(i);
		cin >> i;
	}
	//write your code here
    bool flag = l.SearchK(4, i);
    if(flag) cout<<i<<endl;
	return 0;
}