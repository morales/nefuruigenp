/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-04 20:01:53
 * @LastEditTime: 2020-10-06 18:39:57
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
#include<stdlib.h>
using namespace std;

const int defaultSize = 10;

class SeqList {
protected:
    int *data;
	int maxSize; //表最大可容纳项数
	int last; //当前表大小
	void reSize(int newSize); //改变data数组空间大小
public:
	SeqList(int sz = defaultSize);
	SeqList(SeqList &l);
	~SeqList();
	int Size() const; //计算表最大可容纳表项个数
	int Length() const; //计算表长度
	int Search(int &x) const; //搜索x在表中位置，函数返回表项序号
	bool getData(int i,int &x) const; //取第i歌表项的值，存放在x中
	void setData(int i,int &x); //用x修改第i个表项的值
	bool Insert(int i,int &x); //插入x在第i歌表项之后
	bool Remove(int i,int &x); //删除第i歌表项，用x返回其值
	bool isEmpty(); //判断表是否空
	bool isFull(); //判断表是否满
	void output(); // 输出顺序表
};

//write your code here
SeqList::SeqList(int sz)
{
	if (sz>0) {
		maxSize = sz;
		last = -1;
		data = new int[maxSize];
		if (data == NULL)
			exit(1);
	}
}
SeqList::~SeqList()
{
	delete []data;
}
int SeqList::Size() const
{
	return maxSize;
}
int SeqList::Length() const
{
	return last+1;
}
int SeqList::Search(int &x) const
{
	for(int i=1; i<=last; i++)
		if(data[i] == x) return i;
}
bool SeqList::getData(int i, int &x) const
{
	x = data[i];
	return 1;
}
void SeqList::setData(int i, int &x)
{
	data[i] = x;
}
bool SeqList::Insert(int i, int &x)
{
	if (last == maxSize-1)
		return false;
	if (i<0||i>last+1)
		return false;
	int j;
	for (j=last;j>=i;j--)
		data[j+1] = data[j];
	data[i] = x;
	last++;
    maxSize = last;
	return true;
}
bool SeqList::Remove(int i, int &x)
{
	if(last == -1) return false;
	if(i<1 || i>last) return false;
	x = data[i];
	for(int j=i; i<=last-1; j++)
		data[j] = data[j+1];
	data[last] = 0;
	last--;
	maxSize = last;
	return true;
}
bool SeqList::isEmpty()
{
	if(last == -1) return true;
	else return false;
}
bool SeqList::isFull()
{
	if(last == maxSize-1) return true;
	else return false;
}


// the end
void SeqList::output()
{
    int i;
	for (i=0;i<=last;i++)
		cout << data[i] << " ";
	cout << endl;
}

int main()
{
	SeqList l(8);
	int n,i,x;
	cin >> n;
	for (i=0;i<n;i++) {
		cin >> x;
		if (l.isFull())
			break;
		l.Insert(i,x);
	}
	cout << l.Size() << " " << l.Length() << endl;
	l.output();
	l.Remove(3,n);
	l.setData(2,n);
	l.getData(1,n);
	cout << n << endl;
	l.output();
	return 0;
}