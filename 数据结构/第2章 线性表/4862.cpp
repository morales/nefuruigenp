/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-16 09:08:26
 * @LastEditTime: 2020-10-16 09:08:28
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef int ElemType;        // 定义元素的类型为整型
typedef int Status;                // 定义状态类型
#define ERROR 0
#define OK 1

typedef struct LNode{
        ElemType data;                // 定义数据元素
        struct LNode *next;        // 定义下一节点的链接（指针）
}LNode, *LinkList;                // 定义节点类型以及链表类型（链表类型实际上是一个节点指针类型）

// Write your code
void CreateList_L(LinkList &L, int n) { 
        // 逆位序输入n个元素的值，建立带表头结点的单链线性表L
        LinkList p;
        int i;
        L = (LinkList) malloc(sizeof(LNode));
        L->next = NULL; // 先建立一个带头结点的单链表
        for (i = n; i > 0; --i) {
                p = (LinkList) malloc(sizeof(LNode)); // 生成t新结点
                scanf("%d", &p->data); // 读入元素值
                p->next = L->next;
                L->next = p; // 插入到表头
        }
} // CreateList_L

int ShowList_L(LinkList L){
        // 显示链表中的元素，返回值为链表元素的数目
        int numOfList = 0;    // 记录链表中元素的数目
        LinkList p = L->next;        // 用来遍历链表元素的指针
        while(p){          // 如果该结点不为空
                if(numOfList){    // 如果该元素不是列表中的第一个元素（先判断值是否为，然后加）
                        putchar(' ');
                }
                numOfList++;    // 元素的数目加
                printf("%d",p->data);        // 输出元素
                p = p->next;     // 指针向后移动
        }
        if(numOfList == 0){   // 如果链表中的元素数目为，说明是空链表
                return 0;
        }else{
                putchar('\n');     // 注意换行
                return numOfList;        // 返回链表中元素的数目
        }
}

// The end

int main(){
    LinkList L;
	int n;
	scanf("%d",&n);
	CreateList_L(L,n);//需要写的函数
	ShowList_L(L);    //需要写的函数
	return 0;
}