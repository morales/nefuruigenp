#include<iostream>
#include<stdlib.h>
using namespace std;

const int defaultSize = 10;

class SeqList {
protected:
    int *data;
    int maxSize; //表最大可容纳项数
    int last; //当前表大小
public:
    SeqList(int sz = defaultSize);
    SeqList(SeqList &l);
    ~SeqList();
    int Length() const; //计算表长度
    int getData(int i) const; //取第i歌表项的值，存放在x中
    void setData(int i,int x);
    bool Insert(int i,int x); //插入x在第i歌表项之后
    void Remove(int i);
    void output(); // 输出顺序表
};

SeqList::SeqList(int sz)
{
    if (sz>0) {
        maxSize = sz;
        last = -1;
        data = new int[maxSize];
        if (data == NULL)
            exit(1);
    }
}

SeqList::SeqList(SeqList &l)
{
    maxSize = defaultSize;
    last = l.Length()-1;
    data = new int[maxSize];
    if(data == NULL) exit(1);
    for(int i=1; i<=last+1; i++) data[i-1] = l.getData(i-1);
}

SeqList::~SeqList()
{
    delete []data;
}

int SeqList::Length() const
{
    return last+1;
}

int SeqList::getData(int i) const
{
    return data[i];
}

void SeqList::setData(int i,int x)
{
    data[i] = x;
}

bool SeqList::Insert(int i,int x)
{
    if (last == maxSize-1)
        return false;
    if (i<0||i>last+1)
        return false;
    int j;
    for (j=last;j>=i;j--)
        data[j+1] = data[j];
    data[i] = x;
    last++;
    return true;
}

void SeqList::Remove(int i)
{
    int j;
    for (j=i;j<=last-1;j++)
        data[j] = data[j+1];
    last--;
}

void SeqList::output()
{
    int i;
    for (i=0;i<=last;i++)
        cout << data[i] << " ";
    cout << endl;
}

//write your code here
void CompAComp(SeqList l1, SeqList l2)
{
    int i;
    int x, y;
    // l1.output();
    // l2.output();
    for(i=0; i<min(l1.Length(), l2.Length()); )
    {
        x = l1.getData(i);
        y = l2.getData(i);
        if(x == y)
        {
            cout<<x<<" ";
            l1.Remove(i);
            l2.Remove(i);
        }
        else break;
    }
    cout<<endl;
    //* l1.output();
    //* l2.output();
    //? cout<<l1.Length()<<"sfe"<<l2.Length()<<endl;
    if(!l1.Length() && !l2.Length()) cout<<"=";
    else
    {
        if(!l1.Length())
            cout<<"<";
        else if(l1.Length() && l2.Length())
        {
            x = l1.getData(i);
            y = l2.getData(i);
            //? cout<<x<<"jij"<<y<<endl;
            if(x<y) cout<<"<";
            else cout<<">";
        }
        else cout<<">";
        
    }
    
    cout<<endl;
}

int main()
{
    int n,m,i,x;
    cin >> n >> m;
    SeqList l1(n),l2(m);
    for (i=0;i<n;i++) {
        cin >> x;
        l1.Insert(i,x);
    }
    for (i=0;i<m;i++) {
        cin >> x;
        l2.Insert(i,x);
    }
    //write your code here
    // l1.output();
    // l2.output();
    CompAComp(l1, l2);
    
    return 0;
}

/*
5 5
1 2 3 4 5
1 2 3 4 6
*/