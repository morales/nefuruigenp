/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-04 18:50:00
 * @LastEditTime: 2020-10-04 19:01:49
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef int ElemType;        // 定义元素的类型为整型
typedef int Status;                // 定义状态类型
#define ERROR 0
#define OK 1

typedef struct LNode{
        ElemType data;                // 定义数据元素
        struct LNode *next;        // 定义下一节点的链接（指针）
}LNode, *LinkList;                // 定义节点类型以及链表类型（链表类型实际上是一个节点指针类型）

void CreateList_L(LinkList &l, int n)
{
    LNode *p;
    l = new LNode;
    l->next = NULL;
    while(n--)
    {
        p = new LNode;
        p->next = l->next;
        scanf("%d", &p->data);
        l->next = p;
    }
}
void ShowList_L(LinkList l)
{
    LNode *p = l->next;
    while(p)
    {
        printf("%d ", p->data);
        p = p->next;
    }
    printf("\n");
}
int ListLengthCalc(LinkList L)
{
    LNode *p = L->next;
    int cnt = 0;
    while(p)
    {
        cnt++;
        p = p->next;
    }
    return cnt;
}
void ListInsert_L(LinkList &L, int m, int e)
{
    if(m<1 || m>ListLengthCalc(L))
    {
        printf("INPUT ERROR\n");
        exit(0);
    }
    LNode *p = L->next;
    int cnt = 0;
    while(p)
    {
        if(++cnt == m-1)
        {
            LNode *q = new LNode;
            q->data = e;
            q->next = p->next;
            p->next = q;
            return;
        }
        p = p->next;
    }
}

int main(){
    LinkList L;
	int n,i,e;
	scanf("%d",&n);
	CreateList_L(L,n);
	ShowList_L(L);
	scanf("%d",&i);
	scanf("%d",&e);
	ListInsert_L(L,i,e);     //需要完成的函数
	ShowList_L(L);
	return 0;
}