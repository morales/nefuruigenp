/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-02 23:44:37
 * @LastEditTime: 2020-10-03 00:00:18
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int data;
    struct node *next;
}Lnode, *linklist;
void ListCreate(linklist &head, int n)
{
    Lnode *p = new Lnode;
    p = head;
    head->next = NULL;
    for(int i=1; i<=n; i++)
    {
        int num;
        cin>>num;
        p->next = new Lnode;
        p = p->next;
        p->data = num;
        p->next = NULL;
    }
}
void ElemAdd(linklist &head)
{
    Lnode *p = head->next;
    while(p->next != NULL)
    {
        p->data += p->next->data;
        p = p->next;
    }
}
void ListPrint(linklist head)
{
    Lnode *p = head->next;
    while(p != NULL)
    {
        cout<<p->data<<" ";
        p = p->next;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    linklist head = new Lnode;
    int n;
    cin>>n;
    ListCreate(head, n);
    ElemAdd(head);
    ListPrint(head);
    return 0;
}