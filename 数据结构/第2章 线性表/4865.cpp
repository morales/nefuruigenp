/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-04 18:14:06
 * @LastEditTime: 2020-10-04 18:32:46
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int data;
    struct node *next;
}Lnode, *linklist;
int n, tmp;
Lnode *ListCreateHead()
{
    linklist head = new Lnode;
    head->next = NULL;
    Lnode *p;
    //int n, data;
    cin>>n;
    tmp = n;
    while(n--)
    {
        p = new Lnode;
        p->next = head->next;
        cin>>p->data;
        head->next = p;
    }
    return head;
}
void ElemDelete(linklist &head, int i)
{
    if(i<1 || i>tmp)
    {
        cout<<"INPUT ERROR"<<endl;
        return;
    }
    Lnode *p = head->next;
    int cnt = 0;
    while(p != NULL)
    {
        if(++cnt == i-1)
        {
            Lnode *q = p->next;
            cout<<q->data<<endl;
            p->next = p->next->next;
            delete q;
        }
        p = p->next;
    }
}
void ListPrint(linklist head)
{
    Lnode *p = head->next;
    while(p != NULL)
    {
        cout<<p->data<<" ";
        p = p->next;
        //break;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    linklist head;
    head = ListCreateHead();
    //cout<<tmp<<endl;
    ListPrint(head);
    //system("pause");
    int i;
    cin>>i;
    ElemDelete(head, i);
    ListPrint(head);
    return 0;
}