/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-04 15:27:21
 * @LastEditTime: 2020-10-06 18:52:52
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
#include<stdlib.h>
using namespace std;

const int defaultSize = 10;

class SeqList {
protected:
    int *data;
	int maxSize; //表最大可容纳项数
	int last; //当前表大小
public:
	SeqList(int sz = defaultSize);
	~SeqList();
	int Length() const; //计算表长度
	bool Insert(int i,int &x); //插入x在第i歌表项之后
	void reverse();
	void output(); // 输出顺序表
};

SeqList::SeqList(int sz)
{
	if (sz>0) {
		maxSize = sz;
		last = -1;
		data = new int[maxSize];
		if (data == NULL)
			exit(1);
	}
}

SeqList::~SeqList()
{
	delete []data;
}

int SeqList::Length() const
{
	return last+1;
}

bool SeqList::Insert(int i,int &x)
{
	if (last == maxSize-1)
		return false;
	if (i<0||i>last+1)
		return false;
	int j;
	for (j=last;j>=i;j--)
		data[j+1] = data[j];
	data[i] = x;
	last++;
	return true;
}

void SeqList::reverse()
{
    //write your code here
	for(int i=0,j=last; i<j; i++,j--)
		swap(data[i], data[j]);
}

void SeqList::output()
{
	for(int i=0; i<=last; i++)
		cout<<data[i]<<" ";
	cout<<endl;
}

int main()
{
	int n,i,x;
	cin >> n;
	SeqList l(n);
	for (i=0;i<n;i++) {
		cin >> x;
		l.Insert(i,x);
	}
	l.output();
	l.reverse();
	l.output();
	return 0;
}