#include<iostream>
using namespace std;

struct Node {
    int data;
    Node *next;
};

class List {
private:
    Node *first;
public:
    List();
    ~List();
    void makeEmpty();
    Node *getHead();
    void setHead() { first->next = NULL; }
    int Length();
    void create(int a[],int n);
    void output();
};

List::List()
{
    first = new Node();
    first->next = NULL;
}

List::~List()
{
    makeEmpty();
}

int List::Length()
{
    int i=0;
    Node *temp = first->next;
    while (temp!=NULL) {
        i++;
        temp = temp->next;
    }
    return i;
}

Node *List::getHead()
{
    return first;
}

void List::create(int a[],int n)
{
    makeEmpty();
    Node *temp = first;
    int i;
    for (i=0;i<n;i++) {
        Node *t = new Node();
        t->data = a[i];
        t->next = NULL;
        temp->next = t;
        temp = temp->next;
    }
}

void List::makeEmpty()
{
    Node *q;
    while (first->next!=NULL) {
        q = first->next;
        first->next = q->next;
        delete q;
    }
}

void List::output()
{
    Node *temp = first->next;
    while (temp!=NULL) {
        cout << temp->data << " ";
        temp = temp->next;
    }
}

//write your code here
void mergeList(List &h1, List &h2)
{
    Node *pa, *pb, *pc;	//work pointer
    pa = h1.getHead()->next;
    pb = h2.getHead()->next;
    pc = h1.getHead();
    Node* temp;    //temp pointer to delete
    while(pa != NULL && pb != NULL) 
    {
        if(pa->data <= pb->data) 
        {
            pc->next = pa;
            pc = pa;
            pa = pa->next;
        }/*
        else if(pa->data == pb->data) 
        {
            pc->next = pa;
            pc = pa;
            pa = pa->next;
            //temp = pb;
            pb = pb->next;
            //delete temp;
        }*/
        else 
        {
            pc->next = pb;
            pc = pb;
            pb = pb->next;
        }

    }
    if(pa) 
        pc->next = pa;
    if(pb)
        pc->next = pb;
}
void ListReverse(List &head)
{
    Node *p, *q;
    p = head.getHead()->next;
    head.getHead()->next = NULL;
    while(p != NULL)
    {
        q = p;
        p = p->next;
        q->next = head.getHead()->next;
        head.getHead()->next = q;
    }
    //return head;
}
int main()
{
    List ha,hb;
    int a[8];
    int b[6];
    int i;
    for (i=0;i<8;i++)
        cin >> a[i];
    for (i=0;i<6;i++)
        cin >> b[i];

    ha.create(a,8);
    hb.create(b,6);
    mergeList(ha,hb);
    ListReverse(ha);
    ha.output();
    return 0;
}