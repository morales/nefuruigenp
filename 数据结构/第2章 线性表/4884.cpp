/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-18 19:12:14
 * @LastEditTime: 2020-10-18 19:49:45
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
using namespace std;

struct Node {
    int data;
	Node *next;
};

class List {
private:
	Node *first;
public:
	List();
	~List();
	void makeEmpty();
	int Length();
    //write your code here
    Node* Locate(int n);
    Node* maxl();
    int number(int n);
    void create(int *a, int num);
	void output();
};

List::List()
{
	first = new Node();
	first->next = NULL;
}

List::~List()
{
	makeEmpty();
}

int List::Length()
{
	int i=0;
	Node *temp = first->next;
	while (temp!=NULL) {
		i++;
		temp = temp->next;
	}
	return i;
}

//write your code here
Node* List::Locate(int n)
{
    Node *p = first->next;
    int cnt = 0;
    while(p != NULL)
    {
        cnt++;
        if(cnt == n) return p;
        p = p->next;
    }
    return NULL;
}
Node* List::maxl()
{
    Node *p = first->next;
    int maxn = -0x3f3f3f3f;
    Node *q;
    while(p)
    {
        if(maxn < p->data)
        {
            maxn = p->data;
            q = p;
        }
        p = p->next;
    }
    return q;
}
int List::number(int n)
{
    Node *p = first->next;
    int cnt = 0;
    while(p != NULL)
    {
        if(p->data == n) cnt++;
        p = p->next;
    }
    return cnt;
}
void List::create(int *a, int num)
{
    //List();
    Node *p = first;
    for(int i=0; i<num; i++)
    {
        p->next = new Node;
        p = p->next;
        p->data = a[i];
        p->next = NULL;
    }
}
	
void List::makeEmpty()
{
	Node *q;
	while (first->next!=NULL) {
		q = first->next;
		first->next = q->next;
		delete q;
	}
}

void List::output()
{
	Node *temp = first->next;
	while (temp!=NULL) {
		cout << temp->data << " ";
		temp = temp->next;
	}
}

int main()
{
	List l;
	int a[10];
    int i;
    for (i=0;i<10;i++)
        cin >> a[i];
	l.create(a,10);
	cout << l.Locate(3)->data << endl;
	cout << l.maxl()->data << endl;
	cout << l.number(7) << endl;
	l.output();
	return 0;
}