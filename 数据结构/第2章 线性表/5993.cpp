/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-02 23:20:18
 * @LastEditTime: 2020-10-02 23:38:10
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef int ElemType;
typedef struct LNode
{
    ElemType data;
    struct LNode *next;
}LNode, *LinkList;
void CreateList(LinkList & L, int n)
{
    L = new LNode;
    L->next = NULL;
    LNode *p;
    for(int i=1; i<=n; i++)
    {
        int data;
        scanf("%d", &data);
        p = new LNode;
        p->next = L->next;
        p->data = data;
        L->next = p;
    }
}
void ShowList(LinkList L)
{
    LNode *p = L->next;
    while(p)
    {
        printf("%d ", p->data);
        p = p->next;
    }
}
int main()
{
    //ios::sync_with_stdio(false);
    LinkList L = NULL;
    int n;
    scanf("%d", &n);
    CreateList(L, n);
    ShowList(L);
    return 0;
}
