/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-04 18:33:42
 * @LastEditTime: 2020-10-04 18:41:10
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef int ElemType;        // 定义元素的类型为整型
typedef int Status;                // 定义状态类型
#define ERROR 0
#define OK 1

typedef struct LNode{
        ElemType data;                // 定义数据元素
        struct LNode *next;        // 定义下一节点的链接（指针）
}LNode, *LinkList;                // 定义节点类型以及链表类型（链表类型实际上是一个节点指针类型）

void CreateList_L(LinkList &l, int n)
{
    LNode *p;
    l = new LNode;
    l->next = NULL;
    while(n--)
    {
        p = new LNode;
        p->next = l->next;
        scanf("%d", &p->data);
        l->next = p;
    }
}
void ShowList_L(LinkList l)
{
    LNode *p = l->next;
    while(p)
    {
        printf("%d ", p->data);
        p = p->next;
    }
    printf("\n");
}
bool GetElem_L(LinkList L, int m, int &e)
{
    LNode *p = L->next;
    int cnt = 0;
    while(p)
    {
        if(++cnt == m)
        {
            e = p->data;
            return OK;
        }
        p = p->next;
    }
    return 0;
}

int main(){
    LinkList L;
	int n , m , e;
	scanf("%d",&n);
	CreateList_L(L,n);
	ShowList_L(L);
	scanf("%d",&m);
	if( GetElem_L(L,m,e) )
		printf("%d",e);
	else 
		printf("INPUT ERROR");
	printf("\n");
	return 0;
}