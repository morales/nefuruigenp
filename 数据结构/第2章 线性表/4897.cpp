/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-03 16:26:09
 * @LastEditTime: 2020-10-18 18:18:28
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
using namespace std;

struct Node {
    int data;
    Node *rLink;
    Node *lLink;
};

class DList {
private:
    Node *first;
    Node *rear;
public:
    DList();
    ~DList();
    Node *First();
    Node *Rear();
    void Insert(int val);
    void outputByRLink();
    void changeRear(Node *p);
};

DList::DList()
{
    first = new Node();
    rear = first;
    rear->rLink = first;
    first->lLink = rear;
}

DList::~DList()
{
}

Node *DList::First()
{
    return first;
}

Node *DList::Rear()
{
    return rear;
}

void DList::Insert(int val)
{
    Node *n = new Node();
    n->data = val;
    n->rLink = rear->rLink;
    n->lLink = rear;
    rear->rLink->lLink = n;
    rear->rLink = n;
    rear = rear->rLink;
}

void DList::outputByRLink()
{
    Node *t = first->rLink;
    while(t!=first)
    {
        cout<<t->data<<" ";
        t = t->rLink;
    }
    cout << endl;
}

void DList::changeRear(Node *p)
{
    rear = p;
}

void changeLink(DList &l)
{
    //write your code here
    Node *p = l.First();
    int cnt = 1;
    while(p->rLink != l.First())
    {
        cnt++;
        p = p->rLink;
    }
    cnt--;
    if(!(cnt%2))
    {
        cnt -= 2;
        p = l.Rear()->lLink->lLink;
        while(p != l.First())
        {
            Node *temp = p->lLink;
            if(!(cnt%2))
            {
                p->lLink->rLink = p->rLink;
                p->rLink->lLink = p->lLink;
                l.Rear()->rLink = p;
                l.First()->lLink = p;
                p->lLink = l.Rear();
                p->rLink = l.First();
                l.changeRear(p);
                p = temp;
                cnt--;
            }
            else
            {
                p = p->lLink;
                cnt--;
            }
        }
    }
    else
        while(p != l.First())
        {
            Node *temp = p->lLink;
            if(!(cnt%2))
            {
                p->lLink->rLink = p->rLink;
                p->rLink->lLink = p->lLink;
                l.Rear()->rLink = p;
                l.First()->lLink = p;
                p->lLink = l.Rear();
                p->rLink = l.First();
                l.changeRear(p);
                p = temp;
                cnt--;
            }
            else
            {
                p = p->lLink;
                cnt--;
            }
        }
}

int main()
{
    DList l;
    int val;
    cin>>val;
    while(val != -1) 
    {
        l.Insert(val);
        cin>>val;
    }
    changeLink(l);
    l.outputByRLink();
    return 0;
}
