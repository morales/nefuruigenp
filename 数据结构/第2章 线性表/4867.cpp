/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-04 19:10:46
 * @LastEditTime: 2020-10-04 19:59:00
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int data;
    struct node *next;
}Lnode, *linklist;
void ListCreate(linklist &head, int n)
{
    head = new Lnode;
    head->next = NULL;
    Lnode *p;
    int data;
    while(n--)
    {
        p = new Lnode;
        p->next = head->next;
        //scanf("%d", &p->data);
        cin>>p->data;
        head->next = p;
    }
}
int ListLengthCalc(linklist L)
{
    Lnode *p = L->next;
    int cnt = 0;
    while(p)
    {
        cnt++;
        p = p->next;
    }
    //cout<<cnt<<endl;
    return cnt;
}
int ElemGet(linklist head, int a)
{
    int cnt = 0;
    Lnode *p = head->next;
    while(p)
    {
        if(++cnt == a)
            return p->data;
        p = p->next;
    }
    return -1;
}
bool ElemInsert(linklist &head, int a, int e)
{
    if(a<1 || a>ListLengthCalc(head))
        return 0;
    Lnode *p = head->next;
    int cnt = 0;
    if(a == 1)
    {
        Lnode *q = new Lnode;
        q->data = e;
        q->next = head->next;
        head->next = q;
        return 1;
    }
    while(p)
    {
        if(++cnt==a-1)
        {
            Lnode *q = new Lnode;
            q->data = e;
            q->next = p->next;
            p->next = q;
            return 1;
        }
        p = p->next;
    }
    return 0;
}
bool ElemDelete(linklist &head, int a)
{
    if(a<1 || a>ListLengthCalc(head) || !ListLengthCalc(head))
        return 0;
    Lnode *p = head->next;
    if(a == 1)
    {
        Lnode *q = p;
        p = p->next;
        delete q;
        return 1;
    }
    int cnt = 0;
    while(p)
    {
        if(++cnt==a-1)
        {
            Lnode *q = p->next;
            p->next = p->next->next;
            delete q;
            return 1;
        }
        p = p->next;
    }
    return 0;
}
void ListShow(linklist head)
{
    if(!ListLengthCalc(head))
    {
        cout<<"Link list is empty"<<endl;
        return;
    }
    Lnode *p = head->next;
    while(p)
    {
        cout<<p->data<<" ";
        p = p->next;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    int n, T;
    cin>>n;
    linklist head;
    ListCreate(head, n);
    cin>>T;
    while(T--)
    {
        string str;
        cin>>str;
        if(str[0] == 'g')
        {
            int a;
            cin>>a;
            int tmp = ElemGet(head, a);
            if(tmp != -1) cout<<tmp<<endl;
            else cout<<"get fail"<<endl;
        }
        if(str[0] == 'i')
        {
            int a, e;
            cin>>a>>e;
            bool tmp = ElemInsert(head, a, e);
            if(!tmp) cout<<"insert fail";
            else cout<<"insert OK"; 
            cout<<endl;
        }
        if(str[0] == 'd')
        {
            int a;
            cin>>a;
            bool tmp = ElemDelete(head, a);
            if(tmp) cout<<"delete OK"<<endl;
            else cout<<"delete fail"<<endl;
        }
        if(str[0] == 's')
        {
            ListShow(head);
        }
    }
    return 0;
}