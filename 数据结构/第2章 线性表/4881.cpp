/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-03 22:58:58
 * @LastEditTime: 2020-10-04 15:24:29
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
#include<stdlib.h>
using namespace std;

const int defaultSize = 10;

class SeqList {
protected:
    int *data;
    int maxSize; //表最大可容纳项数
	int last; //当前表大小
public:
	SeqList(int sz = defaultSize);
	~SeqList();
	int Length() const; //计算表长度
	int getData(int i) const; //取第i歌表项的值，存放在x中
	void setData(int i,int x);
	bool Insert(int i,int x); //插入x在第i歌表项之后
	void output(); // 输出顺序表
    //int last;
    //void merge();
    //void SeqList::output2();
};

SeqList::SeqList(int sz)
{
	if (sz>0) {
		maxSize = sz;
		last = -1;
		data = new int[maxSize];
		if (data == NULL)
			exit(1);
	}
}

SeqList::~SeqList()
{
	delete []data;
}

int SeqList::Length() const
{
	return last+1;
}

int SeqList::getData(int i) const
{
	return data[i];
}

void SeqList::setData(int i,int x)
{
	data[i] = x;
}

bool SeqList::Insert(int i,int x)
{
	if (last == maxSize-1)
		return false;
	if (i<0||i>last+1)
		return false;
	int j;
	for (j=last;j>=i;j--)
		data[j+1] = data[j];
	data[i] = x;
	last++;
    //cout<<last<<endl;
    maxSize = last;
	cout<<maxSize<<endl;
	return true;
}

void SeqList::output()
{
    //cout<<last<<endl;
    last = maxSize-1;
	cout<<last<<endl;
	int i;
	for (i=0;i<=last;i++)
		cout << data[i] << " ";
	cout << endl;
}

//write your code here
/*void SeqList::output2()
{
    //cout<<last<<endl;
	int i;
	for (i=0;i<=last;i++)
		cout << data[i] << " ";
	cout << endl;
}*/

int main()
{
	int n,m,i,x;
	cin >> n >> m;
	SeqList l1(n),l2(m);
	for (i=0;i<n;i++) {
		cin >> x;
		l1.Insert(i,x);
	}
	for (i=0;i<m;i++) {
		cin >> x;
		l2.Insert(i,x);
	}
    //l1.output();
    //l2.output();
	SeqList l(m+n);
	// write your code here
    int ii=n, j=m, k=n+m;
    //l.last = m+n;
    while(ii && j)
    {
        //cout<<1<<" ";
        int data1=l1.getData(ii-1), data2=l2.getData(j-1);
        if(data1<data2)
        {
            //l.Insert(k, data2);
            l.setData(k-1, data2);
            //cout<<l.getData(k-1)<<endl;
            j--;
        }
        else
        {
            l.setData(k-1, data1);
            //cout<<l.getData(k-1)<<endl;
            ii--;
        }
        k--;
    }
    while(ii)
    {
        //cout<<1<<" ";
        int data1=l1.getData(ii-1);
        l.setData(k-1, data1);
        //cout<<l.getData(k-1)<<endl;
        ii--;
        k--;
    }
    while(j)
    {
        //cout<<1<<" ";
        int data2=l2.getData(j-1);
        l.setData(k-1, data2);
        //cout<<l.getData(k-1)<<endl;
        j--;
        k--;
    }
	l.output();
	return 0;
}