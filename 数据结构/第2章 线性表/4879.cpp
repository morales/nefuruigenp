/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-04 14:19:54
 * @LastEditTime: 2020-10-04 15:26:36
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
#include<stdlib.h>
using namespace std;

const int defaultSize = 10;

class SeqList {
protected:
    int *data;
	int maxSize; //表最大可容纳项数
	int last; //当前表大小
public:
	SeqList(int sz = defaultSize);
	~SeqList();
	int Length() const; //计算表长度
	int getData(int i) const; //取第i歌表项的值，存放在x中
	void setData(int i,int x);
	bool Insert(int i,int &x); //插入x在第i歌表项之后
	void output(); // 输出顺序表
    friend void condense(SeqList &l);
};

SeqList::SeqList(int sz)
{
	if (sz>0) {
		maxSize = sz;
		last = -1;
		data = new int[maxSize];
		if (data == NULL)
			exit(1);
	}
}

SeqList::~SeqList()
{
	delete []data;
}

int SeqList::Length() const
{
	return last+1;
}

int SeqList::getData(int i) const
{
	return data[i];
}

void SeqList::setData(int i,int x)
{
	data[i] = x;
}

bool SeqList::Insert(int i,int &x)
{
	if (last == maxSize-1)
		return false;
	if (i<0||i>last+1)
		return false;
	int j;
	for (j=last;j>=i;j--)
		data[j+1] = data[j];
	data[i] = x;
	last++;
    maxSize = last;
    //cout<<last<<" "<<maxSize<<endl;
	return true;
}

void SeqList::output()
{
    last = maxSize;
	int i;
	for (i=0;i<=last;i++)
		cout << data[i] << " ";
	cout << endl;
}

void condense(SeqList &l)
{
    //write your code here
    int i=0, j=0;
    //SeqList t;
    int maxn = l.maxSize;
    //cout<<maxn<<endl;
    while(i<=maxn && j<=maxn)
    {
        while(l.data[j] != 0)
        {
            i++;
            j++;
        }
        while(l.data[i] == 0)
            i++;
        if(i <= maxn)
        {
            l.data[j] = l.data[i];
            l.data[i] = 0;
        }
    }
}

int main()
{
	int n,i,x;
	cin >> n;
	SeqList l(n);
	for (i=0;i<n;i++) {
		cin >> x;
		l.Insert(i,x);
	}
	condense(l);
	l.output();
	return 0;
}