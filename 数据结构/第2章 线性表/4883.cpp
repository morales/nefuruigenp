/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-03 16:57:29
 * @LastEditTime: 2020-10-03 22:58:00
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<iostream>
#include<stdlib.h>
using namespace std;

struct Node {
    int data;
	Node *next;
};

class List {
private:
	Node *first;
public:
	List();
	~List();
	void makeEmpty();
	void inputFront(int endTag);
	void output();
};

List::List()
{
	first = new Node();
	first->next = NULL;
}

List::~List()
{
	makeEmpty();
}

void List::makeEmpty()
{
	Node *q;
	while (first->next!=NULL) {
		q = first->next;
		first->next = q->next;
		delete q;
	}
}

void List::inputFront(int endTag)
{
	//write your code here
	int data;
    while(cin>>data)
	{
		if(data == endTag) return;
		Node *p = new Node;
		p->next = first->next;
		p->data = data;
		first->next = p;
	}
}

void List::output()
{
	Node *temp = first->next;
	while (temp!=NULL) {
		cout << temp->data << " ";
		temp = temp->next;
	}
}

int main()
{
	List l;
	l.inputFront(-1); //结束符为-1
	l.output();
	return 0;
}