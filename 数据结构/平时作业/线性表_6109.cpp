/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-01 10:21:25
 * @LastEditTime: 2020-10-06 19:05:39
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int data;
    struct node *next;
}Lnode, *linklist;
void ListCreateSorted(linklist &head, int data)
{
    Lnode *p = head;
    while(p->next!=NULL && p->next->data<data) p = p->next;
    if(p->next == NULL) //ElemInsertBack(head, data);
    {
        Lnode *q = new Lnode;
        q->data = data;
        q->next = NULL;
        p->next = q;
    }
    else
    {
        Lnode *q = new Lnode;
        q->data = data;
        q->next = p->next;
        p->next = q;
    }
}
void ListMergeSorted(linklist &h1, linklist &h2, linklist &head)
{
    Lnode *p1=h1->next, *p2=h2->next, *p;
    head = h1;
    p = head;
    while(p1!=NULL && p2!=NULL)
    {
        if(p1->data <= p2->data)
        {
            if(p1->data == p2->data)
                p2 = p2->next;
            p->next = p1;
            p = p1;
            //p = p->next;
            p1 = p1->next;
        }
        else
        {
            p->next = p2;
            p = p2;
            //p = p->next;
            p2 = p2->next;
        }
    }
    p->next = p1?p1:p2;
    delete h2;
}
void LinkPrint(Lnode *head)
{
    Lnode *p = head;
    p = p->next;
    while(p != NULL)
    {
        cout<<p->data<<" ";
        p = p->next;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    linklist head1=new Lnode, head2=new Lnode, head=new Lnode;
    head1->next = NULL;
    head2->next = NULL;
    head->next = NULL;
    int n;
    while(cin>>n && n)
        ListCreateSorted(head1, n);
    while(cin>>n && n)
        ListCreateSorted(head2, n);
    ListMergeSorted(head1, head2, head);
    LinkPrint(head);
    return 0;
}