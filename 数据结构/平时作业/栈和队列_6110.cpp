/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-06 19:46:08
 * @LastEditTime: 2020-10-08 14:58:31
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    char *top, *base;
    int stacksize;
}SqStack;
void StackInit(SqStack &s)
{
    s.base = new char[100];
    if(!s.base) exit(0);
    s.top = s.base;
    s.stacksize = 100;
}
void ElemPush(SqStack &s, char c)
{
    //if(s.top-s.base == s.stacksize) return 0;
    *s.top++ = c;
    //return 1;
}
void ElemPop(SqStack &s)
{
    //if(s.top == s.base) return 0;
    *--s.top;
    //return 1;
}
char GetTop(SqStack s)
{
    if(s.top != s.base) return *(s.top-1);
}
bool Empty(SqStack s)
{
    if(s.top == s.base) return 1;
    else return 0;
}
int main()
{
    SqStack kh;
    StackInit(kh);
    char x;
    bool flag = 0;
    int cnt[6];
    memset(cnt, 0, sizeof(cnt));
    while(cin>>x && x!='#')
    {
        /*if(x=='(' || x=='[' || x=='{')
        {
            ElemPush(kh, x);
        }
        else if(x==')')
        {
            if(GetTop(kh)!='(' || Empty(kh)) flag = 0;
            if(GetTop(kh) == '(') ElemPop(kh);
        }
        else if(x==']')
        {
            if(GetTop(kh)!='[' || Empty(kh)) flag = 0;
            if(GetTop(kh) == '[') ElemPop(kh);
        }
        else if(x=='}')
        {
            if(GetTop(kh)!='{' || Empty(kh)) flag = 0;
            if(GetTop(kh) == '{') ElemPop(kh);
        }*/
        if(x=='(') cnt[0]++;
        if(x==')') cnt[1]++;
        if(x=='[') cnt[2]++;
        if(x==']') cnt[3]++;
        if(x=='{') cnt[4]++;
        if(x=='}') cnt[5]++;
    }
    if(cnt[0]==cnt[1] && cnt[2]==cnt[3] && cnt[4]==cnt[5]) flag = 1;
    cout<<flag<<endl;
    return 0;
}