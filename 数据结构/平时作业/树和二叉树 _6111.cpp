/*
 * @Author: Morales
 * @Date: 2020-11-17 18:11:16
 * @LastEditTime: 2020-11-17 18:47:59
 * @E-mail: lovexposed@foxmail.com
 * @Powered by: Havoc_Wei
 */
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    char data;
    struct node *lc, *rc;
}BiTNode, *BiTree;
bool flag = 0;
void CreateBiTree(BiTree &tree)
{
    char ch;
    cin>>ch;
    if(ch == '*') tree = NULL;
    else
    {
        tree = new BiTNode;
        tree->data = ch;
        CreateBiTree(tree->lc);
        CreateBiTree(tree->rc);
    }
}
int PrintAncestors(BiTree tree, char x)
{
    if(!tree) return 0;
    if(tree->data == x) return 1;
    if(PrintAncestors(tree->lc, x) || PrintAncestors(tree->rc, x))
    {
        cout<<tree->data<<" ";
        flag = 1;
        return 1;
    }
    return 0;
}
int main()
{
    BiTree Tree;
    CreateBiTree(Tree);
    char x;
    cin>>x;
    if(Tree->data == x) cout<<"没有祖先结点";
    else
    {
        PrintAncestors(Tree, x);
        if(!flag) cout<<x<<"不存在";
    }
    return 0;
}