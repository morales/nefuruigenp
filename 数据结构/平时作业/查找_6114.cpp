/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-23 13:54:23
 * @LastEditTime: 2020-12-23 16:25:51
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
void HashInsert(int *hashlist, int len, int key)
{
    int p = 13;
    int has = key%p;
    if(hashlist[has] == -1) hashlist[has] = key;
    else
    {
        int cnt = 0;
        while(hashlist[has] != -1)
        {
            has = (has+1)%len;
            cnt++;
            if(cnt > len)
                break;
        }
        hashlist[has] = key;
    }
}

int main()
{
    //ios::sync_with_stdio(false);
    int m = 15;
    int *hashlist = new int[m];
    fill(hashlist, hashlist+m, -1);
    int a;
    cin>>a;
    while(a)
    {
        HashInsert(hashlist, m, a);
        cin>>a;
    }
    for(int i=0; i<m; i++)
        cout<<i<<" "<<hashlist[i]<<endl;
    return 0;
}