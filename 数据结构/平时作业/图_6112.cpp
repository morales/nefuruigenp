/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-13 20:12:42
 * @LastEditTime: 2020-12-13 21:19:02
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef int VerTexType;
#define MVNum 1000

typedef struct ArcNode{ // 边结点
    int adjvex; // 该边所指向的顶点的位置
    struct ArcNode *nextarc; // 指向下一条边的指针
    // OtheInfo info;
}ArcNode;
typedef struct VNode{ // 顶点信息
    VerTexType data;
    ArcNode *firstarc; // 指向第一条依附该顶点的边的指针
}VNode, AdjList[MVNum];
typedef struct{
    AdjList vertices;
    int vexnum, arcnum; // 图的当前顶点数和边数
}ALGraph;

int LocateVex(ALGraph G, VerTexType v)
{
    for(int i=0; i<G.vexnum; i++)
        if(G.vertices[i].data == v) return i;
    return -1;
}
void CreateDG(ALGraph &G)
{ // 创建有向图
    scanf("%d%d", &G.vexnum, &G.arcnum);
    for(int i=0; i<G.vexnum; i++)
    {
        G.vertices[i].data = i;
        G.vertices[i].firstarc = NULL;
    }
    for(int k=0; k<G.arcnum; k++)
    {
        int v1, v2;
        scanf("%d,%d", &v1, &v2);
        int i=LocateVex(G, v1), j=LocateVex(G, v2);
        // 确定v1和v2在G中位置，即顶点在G.vertices中的序号
        ArcNode *p=new ArcNode;
        p->adjvex = j;
        p->nextarc = G.vertices[i].firstarc;
        G.vertices[i].firstarc = p;
    }
}

int depth = 0;
bool vis[MVNum];
bool DFS_Path(ALGraph G, int i, int j)
{
    i = LocateVex(G, i);
    j = LocateVex(G, j);
    if(i == j) return true;
    else
    {
        vis[i] = 1;
        ArcNode *p = G.vertices[i].firstarc;
        while(p)		
        {
            depth++;
            if(!vis[p->adjvex] && DFS_Path(G, p->adjvex, j))
                return true;
            else if (vis[p->adjvex]) p = p->nextarc;
        }
    }
    if(depth == 1) return false;
}

int main()
{
    //ios::sync_with_stdio(false);
    ALGraph G;
    CreateDG(G);
    int i, j;
    scanf("%d%d", &i, &j);
    printf("%d\n", DFS_Path(G, i, j));
    return 0;
}