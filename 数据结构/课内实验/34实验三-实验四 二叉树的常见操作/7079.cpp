/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-11-08 10:21:17
 * @LastEditTime: 2020-11-08 15:36:11
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    char ch;
    struct node *lc, *rc;
}BiTnode, *BiTree;
typedef struct snode{
    BiTnode *t;
    struct snode *next;
}StackNode, *LinkStack;
void InitStack(LinkStack &s)
{
    s = NULL;
}
void Push(LinkStack &s, BiTree t)
{
    StackNode *p = new StackNode;
    p->t = t;
    p->next = s;
    s = p;
}
void Pop(LinkStack &s, BiTree &t)
{
    if(s == NULL) return;
    t = s->t;
    StackNode *p = s;
    s = s->next;
    delete p;
}
void CreateBiTree(BiTree &tree)
{
    char ch;
    cin>>ch;
    if(ch == '@') tree = NULL;
    else
    {
        tree = new BiTnode;
        tree->ch = ch;
        CreateBiTree(tree->lc);
        CreateBiTree(tree->rc);
    }
}
void InOrderTraverse(BiTree tree)
{
    LinkStack s;
    InitStack(s);
    BiTnode *p=tree, *q=new BiTnode;
    while(p || s!=NULL)
    {
        if(p)
        {
            Push(s, p);
            p = p->lc;
        }
        else
        {
            Pop(s, q);
            cout<<q->ch;
            p = q->rc;
        }
        
    }
}
int main()
{
    //ios::sync_with_stdio(false);
    BiTree tree;
    CreateBiTree(tree);
    InOrderTraverse(tree);
    return 0;
}