/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-11-13 08:15:19
 * @LastEditTime: 2020-11-13 22:20:06
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    char data;
    struct node *lc, *rc;
    bool lflag, rflag;
}BiThrNode, *BiThrTree;
BiThrTree pre;
void CreateBiTree(BiThrTree &tree)
{
    char ch;
    cin>>ch;
    if(ch == '@') tree = NULL;
    else
    {
        tree = new BiThrNode;
        if(!tree) exit(-2); //* OVERFLOW
        tree->data = ch;
        CreateBiTree(tree->lc);
        if(tree->lc) tree->lflag = 0;
        CreateBiTree(tree->rc);
        if(tree->rc) tree->rflag = 0;
    }
}
void InThreading(BiThrTree p)
{
    if(p)
    {
        InThreading(p->lc);
        if(!p->lc)
        {
            p->lflag = 1;
            p->lc = pre;
        }
        else p->lflag = 0;
        if(!pre->rc)
        {
            pre->rflag = 1;
            pre->rc = p;
        }
        else pre->rflag = 0;
        pre = p;
        InThreading(p->rc);
    }
}
BiThrNode *InOrderThreading(BiThrTree tree)
{
    BiThrTree Thrt = new BiThrNode;
    if(!Thrt) exit(-2); //* OVERFLOW
    Thrt->lflag = 0;
    Thrt->rflag = 1;
    Thrt->rc = Thrt;
    if(!tree) Thrt->lc = Thrt;
    else
    {
        Thrt->lc = tree;
        pre = Thrt;
        InThreading(tree);
        pre->rc = Thrt;
        pre->rflag = 1;
        Thrt->rc = pre;
    }
    return Thrt;
}
void InOrderTraverse_Thr(BiThrTree t)
{
    BiThrNode *p=t->lc;
    while(p != t)
    {
        while(!p->lflag) p = p->lc;
        cout<<p->data;
        while(p->rflag && p->rc!=t)
        {
            p = p->rc;
            cout<<p->data;
        }
        p = p->rc;
    }
}
int main()
{
    //ios::sync_with_stdio(false);
    pre = new BiThrNode;
    //pre->lc = NULL;
    pre->rc = NULL;
    BiThrTree tree;
    CreateBiTree(tree);
    BiThrTree Thrt = InOrderThreading(tree);
    InOrderTraverse_Thr(Thrt);
    return 0;
}