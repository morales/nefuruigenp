/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-11-08 15:35:22
 * @LastEditTime: 2020-11-08 15:37:11
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    char ch;
    struct node *lc, *rc;
}BiTnode, *BiTree;
void CreateBiTree(BiTree &tree)
{
    char ch;
    cin>>ch;
    if(ch == '@') tree = NULL;
    else
    {
        tree = new BiTnode;
        tree->ch = ch;
        CreateBiTree(tree->lc);
        CreateBiTree(tree->rc);
    }
}
void EndOrderTraverse(BiTree tree)
{
    if(tree)
    {
        EndOrderTraverse(tree->lc);
        EndOrderTraverse(tree->rc);
        cout<<tree->ch;
    }
}
int main()
{
    //ios::sync_with_stdio(false);
    BiTree tree;
    CreateBiTree(tree);
    EndOrderTraverse(tree);
    return 0;
}