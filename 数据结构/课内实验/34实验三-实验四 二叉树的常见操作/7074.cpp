/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-11-06 11:04:14
 * @LastEditTime: 2020-11-06 11:05:27
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    char ch;
    struct node *lc, *rc;
}BiTnode, *BiTree;
void CreateBiTree(BiTree &tree)
{
    char ch;
    cin>>ch;
    if(ch == '@') tree = NULL;
    else
    {
        tree = new BiTnode;
        tree->ch = ch;
        CreateBiTree(tree->lc);
        CreateBiTree(tree->rc);
    }
}
int CalcDepth(BiTree tree)
{
    if(tree == NULL) return 0;
    else
    {
        int m=CalcDepth(tree->lc), n=CalcDepth(tree->rc);
        if(m>n) return m+1;
        else return n+1;
    }
}
int main()
{
    //ios::sync_with_stdio(false);
    BiTree tree;
    CreateBiTree(tree);
    cout<<CalcDepth(tree)<<endl;
    return 0;
}