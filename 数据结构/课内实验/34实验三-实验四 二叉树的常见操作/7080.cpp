/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-11-05 17:23:59
 * @LastEditTime: 2020-11-06 11:01:10
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    char ch;
    struct node *lc, *rc;
}BiTnode, *BiTree;
void CreateBiTree(BiTree &tree)
{
    char ch;
    cin>>ch;
    if(ch == '@') tree = NULL;
    else
    {
        tree = new BiTnode;
        tree->ch = ch;
        CreateBiTree(tree->lc);
        CreateBiTree(tree->rc);
    }
}
int CountForestLeaf(BiTree tree)
{
    int cnt = 0;
    if(tree)
    {
        if(tree->lc==NULL) cnt++;
       // else
        //{
            cnt += CountForestLeaf(tree->lc);
            cnt += CountForestLeaf(tree->rc);
        //}
        
    }
    return cnt;
}/*
void First(BiTree tree)
{
    if(tree)
    {
        cout<<tree->ch;
        First(tree->lc);
        First(tree->rc);
    }
}*/
int main()
{
    //ios::sync_with_stdio(false);
    BiTree tree;
    CreateBiTree(tree);
    cout<<CountForestLeaf(tree)<<endl;
    //todo First(tree);
    return 0;
}