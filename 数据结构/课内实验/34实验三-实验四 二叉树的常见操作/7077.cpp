/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-11-09 16:16:27
 * @LastEditTime: 2020-11-09 16:42:41
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    char ch;
    struct node *lc, *rc;
}BiTnode, *BiTree;
typedef struct qnode{
    BiTree t;
    struct qnode *next;
}Qnode, *Queuen;
typedef struct{
    Queuen head, tail;
}LinkQueue;

//!
void InitQueue(LinkQueue &q)
{
    q.head = q.tail = new Qnode;
    q.head->next = NULL;
}
bool IsEmpty(LinkQueue q)
{
    if(q.head==q.tail) return 1;
    else return 0;
}
void EnQueue(LinkQueue &q, BiTree tree)
{
    Qnode *p = new Qnode;
    p->t = tree;
    p->next = NULL;
    q.tail->next = p;
    q.tail = p;
}
void Dequeue(LinkQueue &q, BiTree &tree)
{
    if(IsEmpty(q)) return;
    Qnode *p = q.head->next;
    tree = p->t;
    q.head->next = p->next;
    if(q.tail == p) q.tail = q.head;
    delete p;
}

//?
void CreateBiTree(BiTree &tree)
{
    char ch;
    cin>>ch;
    if(ch == '@') tree = NULL;
    else
    {
        tree = new BiTnode;
        tree->ch = ch;
        CreateBiTree(tree->lc);
        CreateBiTree(tree->rc);
    }
}
void LevelOrder(BiTree tree)
{
    LinkQueue q;
    InitQueue(q);
    if(tree != NULL) EnQueue(q, tree);
    while(!IsEmpty(q))
    {
        Dequeue(q, tree);
        cout<<tree->ch;
        if(tree->lc) EnQueue(q, tree->lc);
        if(tree->rc) EnQueue(q, tree->rc);
    }
}
int main()
{
    //ios::sync_with_stdio(false);
    BiTree Tree;
    CreateBiTree(Tree);
    LevelOrder(Tree);
    return 0;
}