/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-17 17:14:31
 * @LastEditTime: 2020-12-17 17:23:38
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;

void HashInsert(int *hashlist, int len, int key)
{
    int p = 0;
    for(int i=len; i>=1; i--)
    {
        bool flag = false;
        for(int j=i-1; j>1; j--)
            if(i%j == 0) flag = true;
        if(!flag)
        {
            p = i;
            break;
        }
    }
    int has = key%p;
    if(!hashlist[has]) hashlist[has] = key;
    else
    {
        int cnt = 0;
        while(hashlist[has])
        {
            has = (has+1)%len;
            cnt++;
            if(cnt > len)
                break;
        }
        hashlist[has] = key;
    }
}

int main()
{
    //ios::sync_with_stdio(false);
    int m;
    cin>>m;
    int *hashlist = new int[m];
    fill(hashlist, hashlist+m, 0);
    int a;
    cin>>a;
    while(a)
    {
        HashInsert(hashlist, m, a);
        cin>>a;
    }
    for(int i=0; i<m; i++)
        cout<<i<<"  ";
    cout<<endl;
    for(int i=0; i<m; i++)
        cout<<hashlist[i]<<"  ";
    cout<<endl;
    return 0;
}