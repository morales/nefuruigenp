/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-17 16:17:01
 * @LastEditTime: 2020-12-17 17:04:19
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct{
    int key;
    // InfoType otherInfo;
}ElemType;
typedef struct BSTNode{
    ElemType data;
    struct BSTNode *lchild, *rchild;
}BSTNode, *BSTree;

void InsertBST(BSTree &T, ElemType e)
{
    if(!T)
    {
        BSTree S = new BSTNode;
        S->data = e;
        S->lchild = S->rchild = NULL;
        T = S;
    }
    else if(e.key < T->data.key)
        InsertBST(T->lchild, e);
    else if(e.key > T->data.key)
        InsertBST(T->rchild, e);
}

void CreateBST(BSTree &T)
{
    T = NULL;
    ElemType e;
    cin>>e.key;
    while(e.key != 0)
    {
        InsertBST(T, e);
        cin>>e.key;
    }
}

void InOrderTraverse(BSTree T)
{
    if(T)
    {
        InOrderTraverse(T->lchild);
        cout<<T->data.key<<" ";
        InOrderTraverse(T->rchild);
    }
    //cout<<endl;
}

bool SearchBST(BSTree T, int key)
{
    if(!T) return 0;
    else if(key == T->data.key) return 1;
    else if(key < T->data.key) return SearchBST(T->lchild, key);
    else return SearchBST(T->rchild, key);
}

void DeleteBST(BSTree &T, int key)
{
    BSTNode *p=T, *f=NULL;
    while(p)
    {
        if(p->data.key == key) break;
        f = p;
        if(p->data.key > key) p = p->lchild;
        else p = p->rchild;
    }
    if(!p) return;
    BSTNode *q = p;
    if(p->lchild && p->rchild)
    {
        BSTNode *s = p->lchild;
        while(s->rchild)
        {
            q = s;
            s = s->rchild;
        }
        p->data = s->data;
        if(q != p) q->rchild = s->lchild;
        else q->lchild = s->lchild;
        delete s;
        return;
    }
    else if(!p->lchild) p = p->rchild;
    else if(!p->rchild) p = p->lchild;
    if(!f) T = p;
    else if(q == f->lchild) f->lchild = p;
    else f->rchild = p;
    delete q;
}

int main()
{
    //ios::sync_with_stdio(false);
    BSTree T;
    CreateBST(T);
    InOrderTraverse(T);
    cout<<endl;
    int key;
    cin>>key;
    if(SearchBST(T, key)) cout<<"该数存在"<<endl;
    else cout<<"该数不存在"<<endl;
    cin>>key;
    DeleteBST(T, key);
    InOrderTraverse(T);
    cout<<endl;
    return 0;
}