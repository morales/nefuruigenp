/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-16 22:23:35
 * @LastEditTime: 2020-12-16 22:38:02
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
int a[100];
int main()
{
    //ios::sync_with_stdio(false);
    int n=0, x, cnt=0;
    while(cin>>x && x)
        a[++n] = x;
    sort(a+1, a+n+1);
    cin>>x;
    // // for(int i=1; i<=n; i++)
    // //     cout<<a[i]<<" ";
    // // cout<<endl;
    int L=1, R=n;
    while(L<=R)
    {
        cnt++;
        int mid = (L+R)/2;
        // cout<<mid<<" "<<a[mid]<<endl;
        if(a[mid]<x) L = mid+1;
        else if(a[mid]>x) R = mid-1;
        else
        {
            cout<<mid<<" "<<cnt<<endl;
            return 0;
        }
    }
    cout<<0<<" "<<cnt<<endl;
    return 0;
}