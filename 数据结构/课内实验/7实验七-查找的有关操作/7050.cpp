/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-17 19:08:06
 * @LastEditTime: 2020-12-17 19:16:45
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int data;
    struct node *next;
}LNode, *LinkList;
typedef struct HashList{
    int len;
    LinkList List;
}Hashlist;

void HashInsert(Hashlist HL, int key)
{
    int p = 0;
    for(int i=HL.len; i>=1; i--)
    {
        bool flag = 0;
        for(int j=i-1; j>1; j--)
            if(!i%j) flag = 1;
        if(!flag)
        {
            p = i;
            break;
        }
    }
    int has = key%p;
    if(!HL.List[has].data) HL.List[has].data = key;
    else
    {
        LNode *p = new LNode;
        p->data = HL.List[has].data;
        p->next = HL.List[has].next;
        HL.List[has].data = key;
        HL.List[has].next = p;
    }
    
}

int main()
{
    //ios::sync_with_stdio(false);
    HashList L;
    L.len = 13;
    L.List = new LNode[13];
    for(int i=0; i<13; i++)
    {
        L.List[i].data = 0;
        L.List[i].next = NULL;
    }
    int key;
    cin>>key;
    while(key)
    {
        HashInsert(L, key);
        cin>>key;
    }
    for(int i=0; i<13; i++)
    {
        cout<<i<<":";
        if(L.List[i].data) cout<<L.List[i].data<<" ";
        LNode *p = L.List[i].next;
        while(p)
        {
            cout<<p->data<<" ";
            p = p->next;
        }
        cout<<endl;
    }
    return 0;
}