/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-23 13:47:11
 * @LastEditTime: 2020-12-23 13:50:25
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
#define maxsize 20
typedef int KeyType;
typedef struct{
    KeyType key;
    // InfoType otherinfo;
}RedType;
typedef struct{
    RedType r[maxsize+1]; // r[0]闲置或用作哨兵单元
    int len;
}SqList;

void BubbleSort(SqList &L)
{
    int m = L.len-1;
    bool flag = true;
    while(m>0 && flag)
    {
        flag = false;
        for(int i=1; i<=m; i++)
            if(L.r[i].key > L.r[i+1].key)
            {
                flag = true;
                swap(L.r[i], L.r[i+1]);
            }
        m--;
    }
}

int main()
{
    //ios::sync_with_stdio(false);
    SqList L;
    int x, len=0;
    len++;
    while(cin>>x && x)
    {
        L.r[len].key = x;
        len++;
    }
    L.len = len-1;
    BubbleSort(L);
    for(int i=1; i<len; i++)
        cout<<L.r[i].key<<" ";
    return 0;
}