/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-23 11:05:44
 * @LastEditTime: 2020-12-23 11:24:53
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
#define maxsize 20
typedef int KeyType;
typedef struct{
    KeyType key;
    // InfoType otherinfo;
}RedType;
typedef struct{
    RedType r[maxsize+1]; // r[0]闲置或用作哨兵单元
    int len;
}SqList;

void HeapAdjust(SqList &L, int s, int m)
{ // 假设r[s+1..m]已经是堆，将r[s..m]调整为以r[s]为根的大根堆
    RedType rc = L.r[s];
    for(int i=2*s; i<=m; i*=2)
    {
        if(i<m && L.r[i].key<L.r[i+1].key) i++;
        if(rc.key >= L.r[i].key) break;
        L.r[s] = L.r[i];
        s = i;
    }
    L.r[s] = rc;
}

void CreatHeap(SqList &L)
{ // 把无序序列L.r[1..n]建成大根堆
    int n = L.len;
    for(int i=n/2; i>0; i--)
        HeapAdjust(L, i, n);
}

void HeapSort(SqList &L)
{
    CreatHeap(L);
    for(int i=L.len; i>1; i--)
    {
        RedType x = L.r[1];
        L.r[1] = L.r[i];
        L.r[i] = x;
        HeapAdjust(L, 1, i-1);
    }
}

int main()
{
    //ios::sync_with_stdio(false);
    SqList L;
    int x, len=0;
    len++;
    while(cin>>x && x)
    {
        L.r[len].key = x;
        len++;
    }
    L.len = len;
    HeapSort(L);
    for(int i=2; i<=len; i++)
        cout<<L.r[i].key<<" ";
    return 0;
}