/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-23 12:52:30
 * @LastEditTime: 2020-12-23 13:46:20
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
#define maxsize 20
typedef int KeyType;
typedef struct{
    KeyType key;
    // InfoType otherinfo;
}RedType;
typedef struct{
    RedType r[maxsize+1]; // r[0]闲置或用作哨兵单元
    int len;
}SqList;

void ShellInsert(SqList &L, int dk)
{
    for(int i=dk+1; i<=L.len; i++)
        if(L.r[i].key < L.r[i-dk].key)
        {
            L.r[0] = L.r[i];
            int j;
            for(j=i-dk; j>0&&L.r[0].key<L.r[j].key; j-=dk)
                L.r[j+dk] = L.r[j];
            L.r[j+dk] = L.r[0];
        }
}

void ShellSort(SqList &L, int *dt, int t)
{
    for(int k=0; k<t; k++)
        ShellInsert(L, dt[k]);
}

int main()
{
    //ios::sync_with_stdio(false);
    SqList L;
    int x, len=0, dt[3]={5, 3, 1};
    len++;
    while(cin>>x && x)
    {
        L.r[len].key = x;
        len++;
    }
    L.len = len-1;
    ShellSort(L, dt, 3);
    for(int i=1; i<len; i++)
        cout<<L.r[i].key<<" ";
    return 0;
}