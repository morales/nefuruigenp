/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-23 11:35:48
 * @LastEditTime: 2020-12-23 12:03:40
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
#define maxsize 20
typedef int KeyType;
typedef struct{
    KeyType key;
    // InfoType otherinfo;
}RedType;
typedef struct{
    RedType r[maxsize+1]; // r[0]闲置或用作哨兵单元
    int len;
}SqList;

int Partition(SqList &L, int low, int high)
{
    L.r[0] = L.r[low];
    int pivotkey = L.r[low].key;
    while(low<high)
    {
        while(low<high && L.r[high].key>=pivotkey)
            high--;
        L.r[low] = L.r[high];
        while(low<high && L.r[low].key<=pivotkey)
            low++;
        L.r[high] = L.r[low];
    }
    L.r[low] = L.r[0];
    return low;
}

void QuickSort(SqList &L, int low, int high)
{
    // int low = 1, high = L.len;
    if(low<high)
    {
        int pivotloc = Partition(L, low, high);
        QuickSort(L, low, pivotloc-1);
        QuickSort(L, pivotloc+1, high);
    }
}

int main()
{
    //ios::sync_with_stdio(false);
    SqList L;
    int x, len=0;
    len++;
    while(cin>>x && x)
    {
        L.r[len].key = x;
        len++;
    }
    //cout<<len<<endl;
    L.len = len-1;
    QuickSort(L, 1, L.len);
    for(int i=1; i<len; i++)
        cout<<L.r[i].key<<" ";
    return 0;
}