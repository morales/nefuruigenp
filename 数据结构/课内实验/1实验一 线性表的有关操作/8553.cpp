/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-09-30 23:04:41
 * @LastEditTime: 2020-10-01 11:06:16
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node
{
    int data;
    struct node *next;
}Lnode, *linklist;
Lnode *LinkCreate()
{
    linklist head;
    head = new Lnode;
    int n;
    Lnode *p;
    p = head;
    p->next = NULL;
    while(cin>>n && n)
    {
        p->next = new Lnode;
        p = p->next;
        p->data = n;
        p->next = NULL;
    }
    return head;
}
void LinkPrint(Lnode *head)
{
    Lnode *p = head;
    p = p->next;
    while(p != NULL)
    {
        cout<<p->data<<" ";
        p = p->next;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    linklist head;
    head = new Lnode;
    head = LinkCreate();
    LinkPrint(head);
    return 0;
}
/*
#include<stdio.h>
//#include<conio.h>
typedef struct Lnode
{
    int data;
    struct Lnode *next;
} node,*link;

int main()
{
    link p,h;
    h=new node;
    p=h;
    h->next=NULL;
    //p = p->next;
    int n;
    while(scanf("%d", &n) != -1)
    {
        p->next = new node;
        p = p->next;
        //scanf("%d",&p->data);
        p->data = n;
        p->next = NULL;
        //if(getch()=='\r')
        //break;

    }

    p=h;
    p=p->next;
    while(p->data && p->next)
    {
        printf("%d ",p->data);
        p=p->next;
    }
    return 0;
}
*/