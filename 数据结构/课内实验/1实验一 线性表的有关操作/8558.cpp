/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-01 11:09:35
 * @LastEditTime: 2020-10-01 13:24:05
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node
{
    int data;
    struct node *pre, *next;
}Lnode, *linklist;
int tot;
void ElemInsert(linklist &head, int data)
{
    Lnode *tmp=new Lnode, *p=head;
    tmp->data = data;
    tmp->next = NULL;
    while(p->next != NULL)
        p = p->next;
    tmp->pre = p;
    p->next = tmp;
}
void ListCreate(linklist &head)
{
    int n;
    while(cin>>n && n)
    {
        ElemInsert(head, n);
        tot++;
    }
}
void opera(linklist &h1, linklist &h2)
{
    Lnode *p, *q, *tmp;
    p = h1->next;
    q = h2;
    for(int i=0; i<tot&&p!=NULL; i++)
    {
        if(p->data%2)
        {
            if(p->next == NULL)
            {
                p->pre->next = NULL;
                q->next = p;
                break;
            }
            p->pre->next = p->next;
            p->next->pre = p->pre;
            q->next = p;
            q->next->pre = q;
            tmp = p->next;
            p->next = NULL;
            p = tmp;
            q = q->next;
        }
        else p = p->next;
    }
}
void LinkPrint(Lnode *head)
{
    Lnode *p = head;
    p = p->next;
    while(p != NULL)
    {
        cout<<p->data<<" ";
        p = p->next;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    linklist head=new Lnode, headeven=new Lnode;
    ListCreate(head);
    opera(head, headeven);
    LinkPrint(headeven);
    LinkPrint(head);
    return 0;
}