/*
 * @ProblemId: 8559
 * @Probtitle: 
 * @Location: http://222.27.166.246
 * @Author: Morales
 * @Date: 2020-09-30 18:50:12
 * @LastEditTime: 2020-10-01 09:49:58
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int data;
    struct node *next;
}Lnode, *Llist;
int n, x;
Lnode* ListCreate(int num)
{
    Llist head;
    Lnode *p;
    head = new Lnode;
    p = head;
    head->next = NULL;
    //cin>>n;
    for(int i=1; i<=num; i++)
    {
        p->next = new Lnode;
        p = p->next;
        scanf("%d", &p->data);
        p->next = NULL;
    }
    return head;
}
Lnode* listreverse(Lnode *head)
{
    Lnode *p, *q;
    p = head->next;
    head->next = NULL;
    while(p != NULL)
    {
        q = p;
        p = p->next;
        q->next = head->next;
        head->next = q;
    }
    return head;
}
void linkprint(Lnode *head)
{
    Lnode *p = head;
    p = p->next;
    while(p != NULL)
    {
        cout<<p->data<<" ";
        p = p->next;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    Llist head, tmphead;
    //Lnode *p;
    head = new Lnode;
    //p = new Lnode;
    head->next = NULL;
    cin>>n;
    head = ListCreate(n);
    tmphead = head;
    linkprint(tmphead);
    head = listreverse(head);
    linkprint(head);
    return 0;
}