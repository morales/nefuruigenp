/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-01 13:25:27
 * @LastEditTime: 2020-10-01 13:56:34
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int coef, expn;
    struct node *next;
}Lnode, *linklist;
void CreatePolyn(linklist &P, int n)
{
    P = new Lnode;
    P->next = NULL;
    for(int i=1; i<=n; i++)
    {
        Lnode *s = new Lnode;
        Lnode *pre, *q;
        //cin>>s->coef>>s->expn;
        scanf("%d,%d", &s->coef, &s->expn);
        pre = P;
        q = P->next;
        while(q && q->expn<s->expn)
        {
            pre = q;
            q = q->next;
        }
        s->next = q;
        pre->next = s;
    }
}
void AddPolyn(linklist pa, linklist pb)
{
    Lnode *p1, *p2, *p3;
    p1 = pa->next;
    p2 = pb->next;
    p3 = pa;
    while(p1 && p2)
    {
        if(p1->expn == p2->expn)
        {
            int sum = p1->coef+p2->coef;
            if(sum != 0)
            {
                p1->coef = sum;
                p3->next = p1;
                p3 = p1;
                p1 = p1->next;
                Lnode *tmp = p2;
                p2 = p2->next;
                delete tmp;
            }
            else
            {
                Lnode *tmp = p1;
                p1 = p1->next;
                delete tmp;
                Lnode *r = p2;
                p2 = p2->next;
                delete r;
            }
        }
        else if(p1->expn < p2->expn)
        {
            p3->next = p1;
            p3 = p1;
            p1 = p1->next;
        }
        else
        {
            p3->next = p2;
            p3 = p2;
            p2 = p2->next;
        }
    }
    p3->next = p1?p1:p2;
    delete pb;
}
void ListPrint(Lnode *head)
{
    Lnode *p = head;
    p = p->next;
    while(p != NULL)
    {
        cout<<p->coef<<"*x^"<<p->expn<<" ";
        p = p->next;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    int n, m;
    linklist head1, head2;
    scanf("%d", &n);
    CreatePolyn(head1, n);
    scanf("%d", &m);
    CreatePolyn(head2, m);
    AddPolyn(head1, head2);
    ListPrint(head1);
    return 0;
}