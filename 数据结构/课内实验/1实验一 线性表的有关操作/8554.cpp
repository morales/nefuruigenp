/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-01 09:11:14
 * @LastEditTime: 2020-10-01 10:00:16
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node
{
    int data;
    struct node *next;
}Lnode, *linklist;
Lnode *LinkCreate()
{
    linklist head;
    head = new Lnode;
    int n;
    Lnode *p;
    p = head;
    p->next = NULL;
    while(cin>>n && n)
    {
        p->next = new Lnode;
        p = p->next;
        p->data = n;
        p->next = NULL;
    }
    return head;
}
Lnode* ListReverse(linklist head)
{
    Lnode *p, *q;
    p = head->next;
    head->next = NULL;
    while(p != NULL)
    {
        q = p;
        p = p->next;
        q->next = head->next;
        head->next = q;
    }
    return head;
}
void LinkPrint(Lnode *head)
{
    Lnode *p = head;
    p = p->next;
    while(p != NULL)
    {
        cout<<p->data<<" ";
        p = p->next;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    linklist head;
    head = new Lnode;
    head = LinkCreate();
    Lnode *p;
    //LinkPrint(head);
    ListReverse(head);
    LinkPrint(head);
    return 0;
}