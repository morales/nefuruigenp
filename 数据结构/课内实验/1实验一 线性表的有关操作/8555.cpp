/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-01 09:28:25
 * @LastEditTime: 2020-10-01 09:49:19
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int data;
    struct node *next;
}Lnode, *linklist;
/*
void ElemInsertBack(linklist &head, int num)
{
    Lnode *q = new Lnode;
    q->data = num;
    q->next = NULL;
    Lnode *p = head;
    p = p->next;
    while(p != NULL) p = p->next;
    p = q;
}*/
void ListCreateSorted(linklist &head, int data)
{
    Lnode *p = head;
    while(p->next!=NULL && p->next->data<data) p = p->next;
    if(p->next == NULL) //ElemInsertBack(head, data);
    {
        Lnode *q = new Lnode;
        q->data = data;
        q->next = NULL;
        p->next = q;
    }
    else
    {
        Lnode *q = new Lnode;
        q->data = data;
        q->next = p->next;
        p->next = q;
    }
}
void linkprint(Lnode *head)
{
    Lnode *p = head;
    p = p->next;
    while(p != NULL)
    {
        cout<<p->data<<" ";
        p = p->next;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    int n;
    linklist head = new Lnode;
    head->next = NULL;
    while(cin>>n && n)
    {
        ListCreateSorted(head, n);
    }
    linkprint(head);
    return 0;
}