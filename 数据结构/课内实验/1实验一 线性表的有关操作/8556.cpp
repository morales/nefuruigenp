/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-01 08:32:41
 * @LastEditTime: 2020-10-01 08:58:19
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node
{
    int data;
    struct node *next;
}Lnode, *linklist;
Lnode *LinkCreate()
{
    linklist head;
    head = new Lnode;
    int n;
    Lnode *p;
    p = head;
    p->next = NULL;
    while(cin>>n && n)
    {
        p->next = new Lnode;
        p = p->next;
        p->data = n;
        p->next = NULL;
    }
    return head;
}
Lnode *ElemDelete(linklist head)
{
    Lnode *p = head;
    //p = p->next;
    while(p->next->next != NULL)
    {
        if((p->next->data)%2 == 0)
        {
            Lnode *q = p->next;
            p->next = p->next->next;
            delete q;
        }
        else p = p->next;
    }
    if(p->next->next == NULL)
        if((p->next->data)%2 == 0)
        {
            Lnode *q = p->next;
            p->next = p->next->next;
            delete q;
        }
    return head;
}
void LinkPrint(Lnode *head)
{
    Lnode *p = head;
    p = p->next;
    while(p != NULL)
    {
        cout<<p->data<<" ";
        p = p->next;
    }
    cout<<endl;
}
int main()
{
    //ios::sync_with_stdio(false);
    linklist head;
    head = new Lnode;
    head = LinkCreate();
    head = ElemDelete(head);
    LinkPrint(head);
    return 0;
}