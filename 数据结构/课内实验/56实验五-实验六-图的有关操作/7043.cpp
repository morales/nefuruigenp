/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-04 08:34:24
 * @LastEditTime: 2020-12-04 09:16:59
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef int VerTexType;
#define MVNum 100
bool visited[MVNum];

typedef struct qnode{
    int vid;
    struct qnode* next;
}que;
que* head = NULL;

typedef struct ArcNode{ // 边结点
    int adjvex; // 该边所指向的顶点的位置
    struct ArcNode *nextarc; // 指向下一条边的指针
    // OtheInfo info;
}ArcNode;
typedef struct VNode{ // 顶点信息
    VerTexType data;
    ArcNode *firstarc; // 指向第一条依附该顶点的边的指针
}VNode, AdjList[MVNum];
typedef struct{
    AdjList vertices;
    int vexnum, arcnum; // 图的当前顶点数和边数
}ALGraph;

int LocateVex(ALGraph G, VerTexType v)
{
    for(int i=0; i<G.vexnum; i++)
        if(G.vertices[i].data == v) return i;
    return -1;
}
void CreateUDG(ALGraph &G)
{ // 创建无向图
    cin>>G.vexnum>>G.arcnum;
    for(int i=0; i<G.vexnum; i++)
    {
        cin>>G.vertices[i].data;
        G.vertices[i].firstarc = NULL;
    }
    for(int k=0; k<G.arcnum; k++)
    {
        int v1, v2;
        cin>>v1>>v2;
        int i=LocateVex(G, v1), j=LocateVex(G, v2);
        // 确定v1和v2在G中位置，即顶点在G.vertices中的序号
        ArcNode *p1=new ArcNode, *p2=new ArcNode;
        p1->adjvex = j;
        p1->nextarc = G.vertices[i].firstarc;
        G.vertices[i].firstarc = p1;
        p2->adjvex = i;
        p2->nextarc = G.vertices[j].firstarc;
        G.vertices[j].firstarc = p2;
    }
}

void BFS(ALGraph G, int v)
{
    ArcNode *curp;
    que *tmp, *newnode;
    if(!visited[v])
    {
        cout<<"v"<<G.vertices[v].data<<" ";
        visited[v] = 1;
    }
    curp = G.vertices[v].firstarc;
    while(curp)
    {
        if(!visited[curp->adjvex])
        {
            cout<<"v"<<G.vertices[curp->adjvex].data<<" ";
            visited[curp->adjvex] = 1;
            if(!head)
            {
                head = new qnode;
                head->vid = curp->adjvex;
                head->next = NULL;
            }
            else
            {
                tmp = head;
                while(tmp->next)
                    tmp = tmp->next;
                newnode = new qnode;
                newnode->vid = curp->adjvex;
                newnode->next = NULL;
                tmp->next = newnode;
            }
        }
        curp = curp->nextarc;
    }
    while(head != NULL)
    {
        tmp = head;
        head = head->next;
        BFS(G, tmp->vid);
        delete tmp;
    }
}
void BFSTraverse(ALGraph G)
{
    for(int i=0; i<G.vexnum; i++)
        visited[i] = 0;
    for(int i=0; i<G.vexnum; i++)
    {
        if(!visited[i]) BFS(G, i);
    }
}

int main()
{
    //ios::sync_with_stdio(false);
    ALGraph G;
    CreateUDG(G);
    BFSTraverse(G);
    return 0;
}
