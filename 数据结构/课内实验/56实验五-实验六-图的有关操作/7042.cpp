/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-11 08:53:28
 * @LastEditTime: 2020-12-11 09:10:33
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef int VerTexType;
#define MVNum 100

typedef struct ArcNode{ // 边结点
    int adjvex; // 该边所指向的顶点的位置
    struct ArcNode *nextarc; // 指向下一条边的指针
    // OtheInfo info;
}ArcNode;
typedef struct VNode{ // 顶点信息
    VerTexType data;
    ArcNode *firstarc; // 指向第一条依附该顶点的边的指针
}VNode, AdjList[MVNum];
typedef struct{
    AdjList vertices;
    int vexnum, arcnum; // 图的当前顶点数和边数
}ALGraph;

int LocateVex(ALGraph G, VerTexType v)
{
    for(int i=0; i<G.vexnum; i++)
        if(G.vertices[i].data == v) return i;
    return -1;
}
void CreateUDG(ALGraph &G)
{ // 创建无向图
    cin>>G.vexnum>>G.arcnum;
    for(int i=0; i<G.vexnum; i++)
    {
        cin>>G.vertices[i].data;
        G.vertices[i].firstarc = NULL;
    }
    for(int k=0; k<G.arcnum; k++)
    {
        int v1, v2;
        cin>>v1>>v2;
        int i=LocateVex(G, v1), j=LocateVex(G, v2);
        // 确定v1和v2在G中位置，即顶点在G.vertices中的序号
        ArcNode *p1=new ArcNode, *p2=new ArcNode;
        p1->adjvex = j;
        p1->nextarc = G.vertices[i].firstarc;
        G.vertices[i].firstarc = p1;
        p2->adjvex = i;
        p2->nextarc = G.vertices[j].firstarc;
        G.vertices[j].firstarc = p2;
    }
}

bool vis[MVNum];
void DFS(ALGraph G, int v)
{
    ArcNode *Stacktmp[MVNum];
    int top = 0;
    vis[v] = 1;
    cout<<"v"<<G.vertices[v].data<<" ";
    ArcNode *p = G.vertices[v].firstarc;
    while(p || top)
    {
        if(p)
        {
            //int w = LocateVex(G, p->adjvex);
            int w = p->adjvex;
            if(!vis[w])
            {
                cout<<"v"<<G.vertices[w].data<<" ";
                vis[w] = 1;
                Stacktmp[top++] = p;
                p = G.vertices[w].firstarc;
            }
            else p = p->nextarc;
        }
        else p = Stacktmp[top--];
    }
}
void DFSTraverse(ALGraph G)
{
    for(int i=0; i<G.vexnum; ++i)
        if(!vis[i]) DFS(G, i);
}
int main()
{
    //ios::sync_with_stdio(false);
    ALGraph G;
    CreateUDG(G);
    DFSTraverse(G);
    return 0;
}