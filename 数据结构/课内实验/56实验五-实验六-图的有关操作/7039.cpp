/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-12-11 08:13:35
 * @LastEditTime: 2020-12-11 08:42:32
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef int VerTexType;
#define MVNum 100

typedef struct ArcNode{ // 边结点
    int adjvex; // 该边所指向的顶点的位置
    struct ArcNode *nextarc; // 指向下一条边的指针
    // OtheInfo info;
}ArcNode;
typedef struct VNode{ // 顶点信息
    VerTexType data;
    ArcNode *firstarc; // 指向第一条依附该顶点的边的指针
}VNode, AdjList[MVNum];
typedef struct{
    AdjList vertices;
    int vexnum, arcnum; // 图的当前顶点数和边数
}ALGraph;

int LocateVex(ALGraph G, VerTexType v)
{
    for(int i=0; i<G.vexnum; i++)
        if(G.vertices[i].data == v) return i;
    return -1;
}
void CreateDG(ALGraph &G)
{ // 创建有向图
    cin>>G.vexnum>>G.arcnum;
    for(int i=0; i<G.vexnum; i++)
    {
        cin>>G.vertices[i].data;
        G.vertices[i].firstarc = NULL;
    }
    for(int k=0; k<G.arcnum; k++)
    {
        int v1, v2;
        cin>>v1>>v2;
        int i=LocateVex(G, v1), j=LocateVex(G, v2);
        // 确定v1和v2在G中位置，即顶点在G.vertices中的序号
        ArcNode *p=new ArcNode;
        p->adjvex = j;
        p->nextarc = G.vertices[i].firstarc;
        G.vertices[i].firstarc = p;
    }
}
void DisplayGraph(ALGraph G)
{ // 输出邻接表
    ArcNode *p;
    for(int i=0; i<G.vexnum; ++i)
    {
        p = G.vertices[i].firstarc;
        cout<<G.vertices[i].data<<":";
        while(p)
        {
            cout<<G.vertices[p->adjvex].data<<" ";
            p = p->nextarc;
        }
        cout<<endl;
    }
}

bool vis[MVNum];
void DFS_AL(ALGraph G, int v)
{
    cout<<v+1<<" ";
    vis[v] = 1;
    ArcNode *p = G.vertices[v].firstarc;
    while(p)
    {
        int w = p->adjvex;
        if(!vis[w]) DFS_AL(G, w);
        p = p->nextarc;
    }
}
void DFSTraverse(ALGraph G)
{
    memset(vis, 0, sizeof(vis));
    for(int i=0; i<G.vexnum; ++i)
        if(!vis[i]) DFS_AL(G, i);
}

int main()
{
    //ios::sync_with_stdio(false);
    ALGraph G;
    CreateDG(G);
    DisplayGraph(G);
    DFSTraverse(G);
    return 0;
}