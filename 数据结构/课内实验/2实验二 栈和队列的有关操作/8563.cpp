/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-06 19:07:10
 * @LastEditTime: 2020-10-06 19:26:44
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int *top, *base;
    int stacksize;
}SqStack;
void StackInit(SqStack &s)
{
    s.base = new int[100];
    if(!s.base) exit(0);
    s.top = s.base;
    s.stacksize = 100;
}
bool ElemPush(SqStack &s, int data)
{
    if(s.top-s.base == s.stacksize) return 0;
    *s.top++ = data;
    return 1;
}
bool ElemPop(SqStack &s, int &data)
{
    if(s.top == s.base) return 0;
    data = *--s.top;
    return 1;
}
int GetTop(SqStack s)
{
    if(s.top != s.base) return *(s.top-1);
}
bool Empty(SqStack s)
{
    if(s.top == s.base) return 1;
    else return 0;
}
int main()
{
    //ios::sync_with_stdio(false);
    int num;
    SqStack s;
    StackInit(s);
    while(cin>>num && num)
        ElemPush(s, num);
    while(ElemPop(s, num))
        cout<<num<<" ";
    return 0;
}