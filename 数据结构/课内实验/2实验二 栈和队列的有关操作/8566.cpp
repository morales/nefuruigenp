/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-06 19:50:11
 * @LastEditTime: 2020-10-06 19:53:20
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int *top, *base;
    int stacksize;
}SqStack;
void StackInit(SqStack &s)
{
    s.base = new int[100];
    if(!s.base) exit(0);
    s.top = s.base;
    s.stacksize = 100;
}
void ElemPush(SqStack &s, int data)
{
    //if(s.top-s.base == s.stacksize) return 0;
    *s.top++ = data;
    //return 1;
}
void ElemPop(SqStack &s, int &data)
{
    //if(s.top == s.base) return 0;
    data = *--s.top;
    //return 1;
}
int GetTop(SqStack s)
{
    if(s.top != s.base) return *(s.top-1);
}
bool Empty(SqStack s)
{
    if(s.top == s.base) return 1;
    else return 0;
}
int main()
{
    //ios::sync_with_stdio(false);
    SqStack s;
    StackInit(s);
    int n, bit;
    cin>>n>>bit;
    cout<<n<<"(10)=";
    while(n)
    {
        ElemPush(s, n%bit);
        n /= bit;
    }
    while(!Empty(s))
    {
        ElemPop(s, n);
        cout<<n;
    }
    cout<<"("<<bit<<")"<<endl;
    return 0;
}