/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-10 19:41:10
 * @LastEditTime: 2020-10-10 20:38:54
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int data;
    struct node *next;
}Qnode, *queueptr;
typedef struct 
{
    queueptr head, tail;
}queuelink;

void InitQueue(queuelink &q)
{
    q.head = q.tail = new Qnode;
    q.head->next = NULL;
}
void EnQueue(queuelink &q, int elem)
{
    Qnode *p = new Qnode;
    p->data = elem;
    p->next = NULL;
    q.tail->next = p;
    q.tail = p; 
}
void DeQueue(queuelink &q)
{
    if(q.head == q.tail)
        exit(0);
    Qnode *p = q.head->next;
    q.head->next = p->next;
    if(q.tail == p) q.tail = q.head;
    delete p;
    //return 1;
}
int GetHead(queuelink q)
{
    if(q.head != q.tail)
        return q.head->next->data;
    else return 0;
}
int main()
{
    //ios::sync_with_stdio(false);
    queuelink q;
    InitQueue(q);
    int n;
    while(cin>>n && n)
        EnQueue(q, n);
    while(GetHead(q))
    {
        cout<<GetHead(q)<<" ";
        DeQueue(q);
    }
    return 0;
}