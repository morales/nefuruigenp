/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-08 15:00:22
 * @LastEditTime: 2020-10-08 18:12:07
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
typedef struct node{
    int data;
    struct node *next;
}Lnode, *stacklist;
void LinkStackCreate(stacklist &head)
{
    int data;
	head=new Lnode;
	head->next=NULL;
    Lnode *p;
	while(cin>>data && data)
	{
		p=new Lnode;
		p->next = head->next;
		p->data = data;
		head->next = p;
	}
}
void LinkStackPrint(stacklist head)
{
    Lnode *p = head->next;
    while(p)
    {
        cout<<p->data<<" ";
        p = p->next;
    }
}
int main()
{
    //ios::sync_with_stdio(false);
    stacklist head;
    LinkStackCreate(head);
    LinkStackPrint(head);
    return 0;
}