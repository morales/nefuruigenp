/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-10-08 18:13:17
 * @LastEditTime: 2020-10-08 18:24:33
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
//#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
const int maxsize = 1e3;
typedef struct{
    int *base, head, tail;
}SqQueue;
void InitQueue(SqQueue &q)
{
    q.base = new int[maxsize];
    if(!q.base) exit(0);
    q.head = q.tail = 0;
}
int QueueLength(SqQueue q)
{
    return (q.tail-q.head+maxsize)%maxsize;
}
void EnQueue(SqQueue &q, int e)
{
    if((q.tail+1)%maxsize == q.head) exit(0);
    q.base[q.tail] = e;
    q.tail = (q.tail+1)%maxsize;
}
void DeQueue(SqQueue &q)
{
    if(q.head == q.tail) exit(0);
    q.head = (q.head+1)%maxsize;
}
int GetHead(SqQueue q)
{
    if(q.head != q.tail) return q.base[q.head];
    else exit(0);
}
bool EnsureEmpty(SqQueue q)
{
    if(q.head == q.tail) return true;
    else return false;
}
int main()
{
    //ios::sync_with_stdio(false);
    SqQueue q;
    InitQueue(q);
    int n;
    while(cin>>n && n)
        EnQueue(q, n);
    while(!EnsureEmpty(q))
    {
        cout<<GetHead(q)<<" ";
        DeQueue(q);
    }
    return 0;
}