/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-03 21:00:22
 * @LastEditTime: 2020-05-03 21:30:13
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
typedef struct node{
    int data;
    struct node *nx;
}nod;
int main()
{
    int n;
    nod *head, *p;
    head = (nod *)malloc(sizeof(nod));
    p = head;
    head->nx = NULL;
    while(~scanf("%d", &n))
    {
        while(n--)
        {
            p->nx = (nod *)malloc(sizeof(nod));
            p = p->nx;
            scanf("%d", &p->data);
            p->nx = NULL;
        }
        p = head;
        p = p->nx;
        while(p != NULL)
        {
            printf("%d  ", p->data);
            p = p->nx;
        }
    }
    return 0;
}