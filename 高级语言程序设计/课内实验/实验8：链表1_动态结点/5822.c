/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-07 15:58:06
 * @LastEditTime: 2020-05-07 16:13:08
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
typedef struct node{
    int data;
    struct node *next;
}listt;
listt* creat_head(int num)
{
    int data;
    struct node *head=(struct node*)malloc(sizeof(struct node)), *p;
    head->next = NULL;
    for(int i=0; i<num; i++)
    {
        scanf("%d", &data);
        p = (struct node*)malloc(sizeof(struct node));
        p->next = head->next;
        p->data = data;
        head->next = p;
    }
    return head;
}
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        listt *head=creat_head(n);
        listt *h=head;
        h = h->next;
        while(h != NULL)
        {
            printf("%d  ", h->data);
            h = h->next;
        }
        printf("\n");
    }
    return 0;
}