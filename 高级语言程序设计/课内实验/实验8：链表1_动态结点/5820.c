/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-03 20:05:26
 * @LastEditTime: 2020-05-03 20:59:16
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
//#include<string.h>
//#include<stdlib.h>
//#include<math.h>
typedef struct node
{
    int data;
    struct node *next;
}LNode;
int main()
{
    struct node *head, *p1;
    head = (struct node*)malloc(sizeof(struct node));
    int i;
    p1 = head;
    head->next = NULL;
    for(i=1; i<=3; i++)
    {
        p1->next = (struct node*)malloc(sizeof(struct node));
        //head = p1;
        p1 = p1->next;
        scanf("%d", &p1->data);
        p1->next = NULL;
    }
    int pos = 2;
    p1 = head;
    while(pos)
    {
        p1 = p1->next;
        //if(p1 == NULL) 
        
        pos--;
    }
    struct node *tmp = (struct node*)malloc(sizeof(struct node));
    tmp->next = p1->next;
    scanf("%d", &tmp->data);
    p1->next = tmp;
    p1 = p1->next;
    printf("Head:");
    tmp = head;
    tmp = tmp->next;
    while(tmp != NULL)
    {
        printf("->%d", tmp->data);
        tmp = tmp->next;
    }
    return 0;
}
