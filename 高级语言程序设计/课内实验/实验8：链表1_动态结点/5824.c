/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-07 16:23:52
 * @LastEditTime: 2020-05-07 16:29:01
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
typedef struct node{
    int data, id;
    struct node *nx;
}nod;
int main()
{
    int n;
    nod *head, *p;
    head = (nod *)malloc(sizeof(nod));
    p = head;
    head->nx = NULL;
    while(~scanf("%d", &n))
    {
        int cnt = 0;
        while(n--)
        {
            p->nx = (nod *)malloc(sizeof(nod));
            p = p->nx;
            scanf("%d", &p->data);
            p->id = ++cnt;
            p->nx = NULL;
        }
        p = head;
        p = p->nx;
        int flag;
        scanf("%d", &flag);
        while(p != NULL)
        {
            //printf("%d  ", p->data);
            if(flag == p->data)
            {
                printf("%d %d\n", p->id, p->data);
                cnt = 0;
                break;
            }
            p = p->nx;
        }
        if(cnt) printf("0\n");
    }
    return 0;
}