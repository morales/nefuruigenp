#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
int n;
double a[55];
void insert(double s, double *p)
{
    p[0] = s;
    for(int i=0; i<=n; i++)
        for(int j=i; j<=n; j++)
            if(*(p+i) > *(p+j))
            {
                double tmp = *(p+i);
                *(p+i) = *(p+j);
                *(p+j) = tmp;
            }
}
int main()
{
    double x;
    while(~scanf("%d", &n))
    {
        scanf("%lf", &x);
        for(int i=1; i<=n; i++) 
            scanf("%lf", &a[i]);
        insert(x, a);
        for(int i=0; i<=n; i++)
            printf("%.2lf ", a[i]);
        printf("\n");
    }
    return 0;
}
/*
6
3.5 2.1 6.3 7.8 9.6 11.35 15.27
*/