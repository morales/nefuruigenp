#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
int n, x, s, a[105], tot=0;
void check(int h, int m, int *p)
{
    if(h >= m) p[++tot] = h;
}
int main()
{
    while(~scanf("%d%d", &n, &x))
    {
        tot = 0;
        memset(a, 0, sizeof(a));
        for(int i=1; i<=n; i++)
        {
            scanf("%d", &s);
            check(s, x, a);
        }
        for(int i=1; i<tot; i++)
            printf("%d ", a[i]);
        printf("%d\n", a[tot]);
    }
    return 0;
}