/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-11 09:59:12
 * @LastEditTime: 2020-05-11 10:07:05
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
double max, tmp;
int main()
{
    int px, py, n, m;
    while(~scanf("%d %d", &n, &m))
    {
        //memset(a, 0, sizeof(a));
        max = -0x3f3f3f3f;
        for(int i=0; i<n*m; i++)
        {
            scanf("%lf", &tmp);
            if(max < tmp)
            {
                max = tmp;
                px = i/m+1;
                py = i%m+1;
            }
        }
        printf("%.2lf %d %d\n", max, px, py);
    }
    return 0;
}