/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-11 10:07:25
 * @LastEditTime: 2020-05-11 10:10:30
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
char a[100], b[100];
int change(int n, int m)
{
    int tot = 0;
    for(int i=n; i<=m; i++)
        b[tot++] = a[i-1];
    return tot;
}
int main()
{
    int n, m;
    while(~scanf("%s %d%d", a, &n, &m))
    {
        int cnt = change(n, m);
        for(int i=0; i<cnt; i++)
            printf("%c", b[i]);
        printf("\n");
    }
    return 0;
}