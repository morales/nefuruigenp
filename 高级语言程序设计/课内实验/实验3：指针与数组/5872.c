#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
void _swap(int *x, int *y)
{
    int tmp = *x;
    *x = *y;
    *y = tmp;
}
void _3sort(int *n)
{
    for(int i=1; i<=3; i++)
        for(int j=i; j<=3; j++)
            if(n[i]>n[j]) _swap(&n[i], &n[j]);
}
int main()
{
    int p[5];
    while(~scanf("%d", &p[1]))
    {
        for(int i=2; i<=3; i++)
            scanf("%d", &p[i]);
        _3sort(p);
        for(int i=1; i<=2; i++)
            printf("%d ", *(p+i));
        printf("%d \n", *(p+3));
    }
    return 0;
}