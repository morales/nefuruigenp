/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-10 17:10:02
 * @LastEditTime: 2020-05-10 17:40:08
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
typedef struct Node{
    unsigned int id;
    char name[20];
    double score[3], sum, ave;
    struct Node *nx;
}node;
node* link_create(int num)
{
    node *head = (node *)malloc(sizeof(node)), *p;
    p = head;
    head->nx = NULL;
    while(num--)
    {
        p->nx = (node *)malloc(sizeof(node));
        p = p->nx;
        scanf("%d %s", &p->id, p->name);
        p->sum = 0;
        for(int i=0; i<3; i++)
        {
            scanf("%lf", &p->score[i]);
            p->sum += p->score[i];
        }
        p->ave = p->sum/3;
        p->nx = NULL;
    }
    return head;
}
int link_delete(node *head, unsigned int id)
{
    node *h=head, *last=head;
    h = h->nx;
    while(h != NULL)
    {
        if(h->id == id)
        {
            last->nx = h->nx;
            return 1;
        }
        last = h;
        h = h->nx;
    }  
    return 0;
}
void link_print(node *head)
{
    node *p = head;
    p = p->nx;
    while(p != NULL)
    {
        printf("%d  %s  %.2lf  %.2lf  %.2lf  %.2lf  %.2lf\n", p->id, p->name, p->score[0], p->score[1], p->score[2], p->ave, p->sum);
        p = p->nx;
    }
}
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        node *head = link_create(n);
        scanf("%d", &n);
        if(link_delete(head, n)) link_print(head);
        else printf("Error\n");
    }
    return 0;
}
