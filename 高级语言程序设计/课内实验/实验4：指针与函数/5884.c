/*
 * @Author: Gehrychiang
 * @LastEditTime: 2020-04-07 13:48:34
 * @Website: www.yilantingfeng.site
 * @E-mail: gehrychiang@aliyun.com
 */
// trad c for rg
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <float.h>
#include <limits.h>
#include <locale.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wchar.h>
#include <wctype.h>
double nt(double round, double (*func)(double), double (*dfunc)(double))
{
    return round - (*func)(round) / (*dfunc)(round);
}
double func1(double a)
{
    return pow(a, 3) - pow(a, 2) - 1;
}
double dfunc1(double a)
{
    return 3 * pow(a, 2) - 2 * a;
}
double func2(double a)
{
    return pow(a, 4) - 3 * a + 1;
}
double dfunc2(double a)
{
    return 4 * pow(a, 3) - 3;
}
double func3(double a)
{
    return a - pow(exp(1), -a);
}
double dfunc3(double a)
{
    return 1 + pow(exp(1), -a);
}
int main()
{
    //freopen("","r",stdin);
    //freopen("","w",stdout);
    int op;
    double rrr, iii;
    while (~scanf("%d", &op))
    {
        scanf("%lf %lf", &rrr, &iii);
        switch (op)
        {
        case 1:
            while (fabs(rrr - nt(rrr, func1, dfunc1)) >= iii)
            {
                rrr = nt(rrr, func1, dfunc1);
            }
            break;
        case 2:
            while (fabs(rrr - nt(rrr, func2, dfunc2)) >= iii)
            {
                rrr = nt(rrr, func2, dfunc2);
            }
            break;
        case 3:
            while (fabs(rrr - nt(rrr, func3, dfunc3)) >= iii)
            {
                rrr = nt(rrr, func3, dfunc3);
            }
            break;
        default:
            break;
        }
        printf("%d %.2e %.5lf\n", op, iii, rrr);
    }
    //fclose(stdin);
    //fclose(stdout);
    return 0;
}