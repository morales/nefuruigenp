/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-08 08:12:01
 * @LastEditTime: 2020-05-08 08:18:01
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
typedef struct node{
    int data;
    struct node *nx;
}Lnode;
Lnode* link_create_head(int n)
{
    int data;
    struct node *head=(struct node*)malloc(sizeof(struct node)), *p;
    head->nx = NULL;
    for(int i=0; i<n; i++)
    {
        scanf("%d", &data);
        p = (struct node*)malloc(sizeof(struct node));
        p->nx = head->nx;
        p->data = data;
        head->nx = p;
    }
    return head;
}
int _count(Lnode *p)
{
    int cnt = 0;
    p = p->nx;
    while(p != NULL)
    {
        if(p->data%2 == 0) cnt++;
        p = p->nx;
    }
    return cnt;
}
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        Lnode *head = link_create_head(n);
        printf("%d\n", _count(head));
    }
    return 0;
}