/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-10 16:07:24
 * @LastEditTime: 2020-05-10 16:14:51
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
typedef struct Node
{
    int data;
    struct Node *nx;
}node;
node* link_create_empty()
{
    node *head=(node *)malloc(sizeof(node));
    head->nx = NULL;
    return head;
}
void link_insert(node *head, int data)
{
    node *h=head, *prev=head;
    h = h->nx;
    while(h != NULL)
    {
        if(h->data > data)
        {
            prev->nx = (node *)malloc(sizeof(node));
            prev->nx->data = data;
            prev->nx->nx = h;
            return;
        }
        prev = h;
        h = h->nx;
    }
    prev->nx = (node *)malloc(sizeof(node));
    prev->nx->data = data;
    prev->nx->nx = NULL;
}
void link_print(node *head)
{
    node *h = head;
    h = h->nx;
    while(h != NULL)
    {
        printf("%d  ", h->data);
        h = h->nx;
    }
    printf("\n");
}
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        int tmp;
        node *head = link_create_empty();
        while(n--)
        {
            scanf("%d", &tmp);
            link_insert(head, tmp);
        }
        link_print(head);
    }
    return 0;
}