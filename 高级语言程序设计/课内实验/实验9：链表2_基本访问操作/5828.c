/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-10 16:07:24
 * @LastEditTime: 2020-05-28 20:17:16
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
typedef struct Node
{
    int data;
    struct Node *nx;
}node;
node* link_create(int num)
{
    node *head=(node *)malloc(sizeof(node)), *p;
    p = head;
    head->nx = NULL;
    for(int i=1; i<=num; i++)
    {
        p->nx = (node *)malloc(sizeof(node));
        p = p->nx;
        scanf("%d", &p->data);
        p->nx = NULL;
    }
    return head;
}
void link_insert(node *head, int data)
{
    node *h=head, *prev=head;
    h = h->nx;
    while(h != NULL)
    {
        if(h->data > data)
        {
            prev->nx = (node *)malloc(sizeof(node));
            prev->nx->data = data;
            prev->nx->nx = h;
            return;
        }
        prev = h;
        h = h->nx;
    }
    prev->nx = (node *)malloc(sizeof(node));
    prev->nx->data = data;
    prev->nx->nx = NULL;
}
void link_print(node *head)
{
    node *h = head;
    h = h->nx;
    while(h != NULL)
    {
        printf("%d  ", h->data);
        h = h->nx;
    }
    printf("\n");
}
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        node *head = link_create(n);
        scanf("%d", &n);
        link_insert(head, n);
        link_print(head);
    }
    return 0;
}