/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-10 15:31:18
 * @LastEditTime: 2020-05-10 15:40:03
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
typedef struct Node
{
    int data;
    struct Node *nx;
}node;
node* link_create(int num)
{
    node *head=(node *)malloc(sizeof(node)), *p;
    p = head;
    head->nx = NULL;
    for(int i=1; i<=num; i++)
    {
        p->nx = (node *)malloc(sizeof(node));
        p = p->nx;
        scanf("%d", &p->data);
        p->nx = NULL;
    }
    return head;
}
void link_delete(node *head, int data)
{
    node *h=head, *prev=head;
    h = h->nx;
    while(h != NULL)
    {
        if(h->data == data)
            prev->nx = h->nx;
        else prev = h;
        h = h->nx;
    }
}
void link_print(node *head)
{
    node *h = head;
    h = h->nx;
    while(h != NULL)
    {
        printf("%d  ", h->data);
        h = h->nx;
    }
    printf("\n");
}
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        node *head = link_create(n);
        scanf("%d", &n);
        link_delete(head, n);
        link_print(head);
    }
    return 0;
}