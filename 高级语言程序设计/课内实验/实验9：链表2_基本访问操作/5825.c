/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-08 08:02:08
 * @LastEditTime: 2020-05-08 08:10:58
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
typedef struct node{
    int data;
    struct node *nx;
}Lnode;
Lnode* link_create(int n)
{
    Lnode *head=(Lnode *)malloc(sizeof(Lnode)), *p;
    p = head;
    head->nx = NULL;
    for(int i=1; i<=n; i++)
    {
        p->nx = (Lnode *)malloc(sizeof(Lnode));
        p = p->nx;
        scanf("%d", &p->data);
        p->nx = NULL;
    }
    return head;
}
void search(Lnode *head, int n)
{
    Lnode *p=head, *prev=head;
    p = p->nx;
    while(p != NULL)
    {
        if(p->data == n)
        {
            if(prev == head) printf("没有前驱\n");
            else printf("%d\n", prev->data);
            return;
        }
        prev = p;
        p = p->nx;
    }
    printf("x不存在\n");
}
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        Lnode *head = link_create(n);
        scanf("%d", &n);
        search(head, n);
    }
    return 0;
}