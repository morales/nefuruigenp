#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEN 81
int main()
{
   void Caesar_transform(char message[], int shift);
   char message[LEN];
   int shift;
   printf("Enter message to be encrypted: ");
   gets(message);
   printf("Enter shift amount (1-25): ");
   scanf("%d",&shift);
   printf("Encrypted message: ");
   Caesar_transform(message, shift);
   printf("%s\n",message);
   return 0;
}
void Caesar_transform(char message[], int shift)
{
//start
    for(int i=0; message[i]; i++)
    {
        if((message[i]>='A'&&message[i]<='Z'-shift) || (message[i]>='a'&&message[i]<='z'-shift))
            message[i] += shift;
        else if((message[i]>='Z'-shift&&message[i]<='Z') || (message[i]>='z'-shift&&message[i]<='z'))
            message[i] -= 26-shift;
    }
//end
}