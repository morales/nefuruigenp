#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
struct node{
    int num, type;
    double score;
    char name[25];
}a[23];
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        int cnt = 0;
        for(int i=1; i<=n; i++)
        {
            scanf("%d%s%d%lf", &a[i].num, a[i].name, &a[i].type, &a[i].score);
            if(a[i].type==1 && a[i].score<60) cnt++;
            if(a[i].type==2 && a[i].score<50) cnt++;
            if(a[i].type==3 && a[i].score<65) cnt++;
        }
        printf("%d\n", cnt);
        for(int i=1; i<=n; i++)
            printf("%d %s %d %.2lf\n", a[i].num, a[i].name, a[i].type, a[i].score);
    }
    return 0;
}