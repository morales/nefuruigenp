#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
struct node{
    int id;
    double sc;
}stu[1005];
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        double sum = 0;
        for(int i=1; i<=n; i++)
        {
            scanf("%d%lf", &stu[i].id, &stu[i].sc);
            sum += stu[i].sc;
        }
        sum /= n;
        for(int i=1; i<=n; i++)
            if(stu[i].sc > sum)
                printf("%8d %.1lf\n", stu[i].id, stu[i].sc);
    }
    return 0;
}