/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-08 08:28:22
 * @LastEditTime: 2020-05-08 08:33:59
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include <stdio.h>
#include <stdlib.h>
#define R 3
#define C 5
#define N (R>C?R:C)
int main(void)
{
   void transfor(int a[][N]);
   int array[N][N],i,j;
   for(i=0;i<R;i++)
      for(j=0;j<C;j++)
         scanf("%d",&array[i][j]);
   transfor(array);
   for(i=0;i<C;i++)
      for(j=0;j<R;j++)
         j!=R-1?printf("%d ",array[i][j]):printf("%d\n",array[i][j]);
    return 0;
}
void transfor(int a[][N])
{
//start
    // int flag[N][N];
    // for(int i=0; i<N; i++)
    //     for(int j=0; j<N; j++)
    //         flag[i][j] = 0;
    for(int i=0; i<5; i++)
        for(int j=i+1; j<5; j++)
        {
            //if(!flag[i][j])
            //{
                int tmp = a[i][j];
                a[i][j] = a[j][i];
                a[j][i] = tmp;
                //flag[i][j] = 1;
            //}
        }
//end
}
/*
1 2 3 4 5
5 1 2 3 4
4 5 1 2 3
*/