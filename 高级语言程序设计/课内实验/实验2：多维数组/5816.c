/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-08 08:35:07
 * @LastEditTime: 2020-05-08 08:35:17
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
char str[6][10] = {"green", "red", "blue", "pink", "orange", "black"};
char balloon[10];
int flagn[6];
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        //cout<<str[0]<<endl;
        memset(flagn, 0, sizeof(flagn));
        for(int i=1; i<=n; i++)
        {
            scanf("%s", balloon);
            if(!strcmp(balloon, str[0])) flagn[0]++;
            if(!strcmp(balloon, str[1])) flagn[1]++;
            if(!strcmp(balloon, str[2])) flagn[2]++;
            if(!strcmp(balloon, str[3])) flagn[3]++;
            if(!strcmp(balloon, str[4])) flagn[4]++;
            if(!strcmp(balloon, str[5])) flagn[5]++;
        }
        int maxn=-11, flag;
        for(int i=0; i<6; i++)
        {
            if(maxn<flagn[i])
            {
                maxn = flagn[i];
                flag = i;
            }
        }
        printf("%s\n", str[flag]);
    }
    return 0;
}
