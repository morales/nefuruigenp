/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-08 08:23:32
 * @LastEditTime: 2020-05-08 08:27:33
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include <stdio.h>
#include <stdlib.h>
#define N 5
int main(void)
{
   void transfor(int a[][N]);
   int array[N][N],i,j;
   for(i=0;i<N;i++)
      for(j=0;j<N;j++)
         scanf("%d",&array[i][j]);
   transfor(array);
   for(i=0;i<N;i++)
      for(j=0;j<N;j++)
         j!=N-1?printf("%d ",array[i][j]):printf("%d\n",array[i][j]);
    return 0;
}
void transfor(int a[][N])
{
//start
    for(int i=0; i<N; i++)
        for(int j=0; j<i; j++)
        {
            int tmp = a[i][j];
            a[i][j] = a[j][i];
            a[j][i] = tmp;
        }
//end
}
/*
1 2 3 4 5
5 1 2 3 4
4 5 1 2 3
3 4 5 1 2
2 3 4 5 1
*/