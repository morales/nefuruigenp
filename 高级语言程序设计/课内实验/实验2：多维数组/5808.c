/*
 * @ProblemId: 
 * @Probtitle: 
 * @Location: 
 * @Author: Morales
 * @Date: 2020-05-08 08:35:55
 * @LastEditTime: 2020-05-08 08:38:42
 * @Space: https://space.bilibili.com/350869102
 * @E-mail: lovexposed@foxmail.com
 * @Blog: https://blog.csdn.net/baidu_41248654
 * @Powered by: Havoc_Wei
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
int cnt = 0;
char ch[11], c;
int main()
{
    while(~scanf("%c", &c))
    {
        if((c>='A'&&c<='Z') || (c>='a'&&c<='z'))
            ch[cnt++] = c;
        else
        {
            if(strlen(ch) == 0) continue;
            printf("%s\n", ch);
            memset(ch, 0, sizeof(ch));
            cnt = 0;
        }
    }
    return 0;
}