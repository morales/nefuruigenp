#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
struct node{
    double x, y;
};
int main()
{
    int n;
    struct node *p, *q;
    while(~scanf("%d", &n))
    {
        p=q=(struct node*)calloc(n, sizeof(struct node));
        for(int i=0; i<n; i++)
        {
            scanf("%lf%lf", &p->x, &p->y);
            p++;
        }
        double minn = 0x3f3f3f3f;
        p = q;
        for(int i=0; i<n; i++)
            for(int j=i+1; j<n; j++)
            {
                double tmp = sqrt(pow(p[j].x-p[i].x, 2)+pow(p[j].y-p[i].y, 2));
                if(minn > tmp) minn = tmp;
            }
        printf("%.1lf\n", minn);
    }
    return 0;
}