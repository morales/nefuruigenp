#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
struct stu{
    char name[20], sex;
    double sg[3], sum;
}dbg[20];
int main()
{
    int n;
    scanf("%d", &n);
    struct stu ans;
    ans.sum = -0x3f3f3f3f;
    for(int i=1; i<=n; i++)
    {
        scanf("%s %c %lf %lf %lf", dbg[i].name, &dbg[i].sex, &dbg[i].sg[0], &dbg[i].sg[1], &dbg[i].sg[2]);
        dbg[i].sum = dbg[i].sg[0]+dbg[i].sg[1]+dbg[i].sg[2];
        if(dbg[i].sex=='f' && ans.sum<dbg[i].sum)
            memcpy(&ans, &dbg[i], sizeof(dbg[i]));
    }
    printf("I choose %s,and her score is %.2lf.", ans.name, ans.sum/3);
    return 0;
}