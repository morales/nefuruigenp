#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
struct node{
    int id;
    char name[24];
    double sc;
}p;
int main()
{
    int n;
    while(~scanf("%d", &n))
    {
        int a=0, b=0, c=0;
        while(n--)
        {
            scanf("%d%s%lf", &p.id, p.name, &p.sc);
            if(p.sc>=80) a++;
            if(p.sc>=60 && p.sc<80) b++;
            if(p.sc<60) c++;
        }
        printf("%d %d %d\n", a, b, c);
    }
    return 0;
}