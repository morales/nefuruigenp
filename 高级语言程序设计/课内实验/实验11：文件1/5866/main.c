#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct stu{
    int id, namelen;
    char name[20], sex;
    double sc[3];
    double sum, ave;
}s[100];
int main()
{
    FILE *fp;
    int n, i, j;
    fp = fopen("student.txt", "w");
    scanf("%d", &n);
    for(i=0; i<n; i++)
    {
        printf("请输入学号：\n");
        scanf("%d", &s[i].id);
        fprintf(fp, "%d", s[i].id);
        getchar();
        printf("请输入姓名：\n");
        //gets(s[i].name);
        //fprintf(fp, " %d %s", strlen(s[i].name), s[i].name);
        char str[20];
        gets(str);
        fprintf(fp, " %d %s", strlen(str), str);
        printf("请输入性别：\n");
        scanf("%c", &s[i].sex);
        fprintf(fp, " %c", s[i].sex);
        printf("请输入成绩：\n");
        double sc[3];
        s[i].sum = 0;
        for(j=0; j<3; j++)
        {
            //scanf("%lf", &s[i].sc[j]);
            //fprintf(fp, " %.2lf", s[i].sc[j]);
            scanf("%lf", &sc[j]);
            //fprintf(fp, " %.2lf", sc[j]);
            s[i].sum += sc[j];
        }
        for(j=0; j<3; j++)
        {
            fprintf(fp, " %.2f", sc[j]);
        }
        s[i].ave = s[i].sum/3;
        fprintf(fp, " %.2f %.2f\n", s[i].sum, s[i].ave);
    }
    fclose(fp);
    return 0;
}
