package com.experiment06;

import java.util.Scanner;

class DangerException extends Exception {
    void show() {
        System.out.println("超重");
    }
}

class CargoBoat {
    int realContent;
    int maxContent;

    public void setMaxContent(int maxContent) {
        this.maxContent = maxContent;
    }

    public void loading(int m) throws DangerException {
        realContent += m;
        if (realContent > maxContent) {
            throw new DangerException();
        } else {
            System.out.println("目前装载了" +realContent + "吨货物");
        }
    }
}

public class Exp62 {
    public static void main(String[] args) {
        CargoBoat cargoBoat = new CargoBoat();
        Scanner scanner = new Scanner(System.in);
        cargoBoat.setMaxContent(1000);
        int m = 0;
        try {
            while (true) {
                m = scanner.nextInt();
                cargoBoat.loading(m);
            }
        } catch (DangerException e) {
            e.show();
            System.out.println("无法再装载重量是" + m +"吨的集装箱");
        } finally {
            System.out.println("货船将正点启航");
        }
    }
}
