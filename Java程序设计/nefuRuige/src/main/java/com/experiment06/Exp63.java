package com.experiment06;

import java.util.InputMismatchException;
import java.util.Scanner;

class TestTriangle {
    static void triangle(int a, int b, int c) throws IllegalArgumentException, InputMismatchException {
        if(a+b<=c || a+c<=b || b+c<=a) {
            throw new IllegalArgumentException();
        }
    }
}

public class Exp63 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = 0, b = 0, c = 0;
        try {
            a = scanner.nextInt();
            b = scanner.nextInt();
            c = scanner.nextInt();
            TestTriangle.triangle(a, b, c);
            System.out.println("三角形的三边长为：" + a + "," + b + "," + c);
        } catch (IllegalArgumentException iae) {
            System.out.println(a + " " + b +  " " + c + "不能构成三角形");
        } catch (InputMismatchException ime) {
            System.out.println("请输入整数作为三角形的边长");
        }
    }
}
