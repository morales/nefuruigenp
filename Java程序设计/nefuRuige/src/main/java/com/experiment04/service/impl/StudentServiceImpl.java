package com.experiment04.service.impl;

import com.experiment04.entity.Student;
import com.experiment04.resource.DatabaseUtils;
import com.experiment04.service.StudentService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StudentServiceImpl implements StudentService {
    @Override
    public List<Student> addStudent(Student student) {
        DatabaseUtils.getStudents().add(student);
        return DatabaseUtils.getStudents();
    }

    @Override
    public List<Student> listStudentsByYear(int year) {
        List<Student> studentList = DatabaseUtils.getStudents();
        List<Student> newList = new ArrayList<Student>();
        for (Student student: studentList) {
            if (student.getYear() == year) {
                newList.add(student);
            }
        }
        return newList;
    }

    @Override
    public List<String> listStudentsNames(int year, Student.Sex sex) {
        return DatabaseUtils.getStudents().stream().filter(s->s.getYear()==year).filter(s->s.getSex()==sex).map(Student::getName).collect(Collectors.toList());
    }

    @Override
    public Map<Student.Sex, List<Student>> mapStudentsBySex() {
        return DatabaseUtils.getStudents().stream().collect(Collectors.groupingBy(Student::getSex));
    }

    @Override
    public boolean removeStudent(int id) {
        return DatabaseUtils.getStudents().removeIf(s->s.getId()==id);
    }
}
