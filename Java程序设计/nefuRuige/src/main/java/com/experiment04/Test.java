package com.experiment04;

import com.experiment04.entity.Student;
import com.experiment04.service.StudentService;
import com.experiment04.service.impl.StudentServiceImpl;

import java.util.List;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        Student student = new Student(201909, Student.Sex.MALE, "莫拉莱斯", 2019);
        testAddStudent(student);
        testListStudentsByYear(2012);
        testListStudentsName(2011, Student.Sex.MALE);
        testMapStudentsBySex();
        testRemoveStudent(201104);

    }

    private static void testAddStudent(Student student) {
        System.out.println("Add Student");
        StudentService studentService = new StudentServiceImpl();
        studentService.addStudent(student).forEach(student1 -> System.out.println(student1.toString()));
        System.out.println("");
    }

    private static void testListStudentsByYear(int year) {
        System.out.println("List Students By Year");
        StudentService studentService = new StudentServiceImpl();
        studentService.listStudentsByYear(year).forEach(student1 -> System.out.println(student1.toString()));
        System.out.println();
    }

    private static void testListStudentsName(int year, Student.Sex sex) {
        System.out.println("List Students Names");
        StudentService studentService = new StudentServiceImpl();
        studentService.listStudentsNames(year, sex).forEach(System.out::println);
        System.out.println();
    }

    private static void testMapStudentsBySex() {
        System.out.println("Map Students By Sex");
        StudentService studentService = new StudentServiceImpl();
        Map<Student.Sex, List<Student>>listMap = studentService.mapStudentsBySex();
        for (Map.Entry<Student.Sex, List<Student>>sexListEntry: listMap.entrySet()) {
            System.out.println(sexListEntry.getKey());
            sexListEntry.getValue().forEach(student -> System.out.println(student.toString()));
        }
        System.out.println();
    }

    private static void testRemoveStudent(int id) {
        System.out.println("Remove Student");
        StudentService studentService = new StudentServiceImpl();
        System.out.println(studentService.removeStudent(id));
    }
}
