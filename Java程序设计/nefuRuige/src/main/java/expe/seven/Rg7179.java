package expe.seven;

class Race implements Runnable {
    private int all = 40;
    private int turtle = 0;
    private int rabbit = 0;

    @Override
    public void run() {
        while (true) {
            if ("Turtle".equals(Thread.currentThread().getName())) {
                turtle += 1;
                if (turtle >= all) {
                    System.out.println("乌龟到达终点");
                    break;
                } else {
                    System.out.println("乌龟距离终点还差" + (all-turtle) + "米");
                }
            } else if ("Rabbit".equals(Thread.currentThread().getName())) {
                rabbit += 10;
                if (rabbit >= all) {
                    System.out.println("兔子到达终点");
                    break;
                } else {
                    System.out.println("兔子距离终点还差" + (all-rabbit) + "米");
                }
                if (rabbit > turtle) {
                    System.out.println("兔子os: 我跑得快，睡一觉");
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}

public class Rg7179 {
    public static void main(String[] args) {
        Race race = new Race();
        Thread turtleThread = new Thread(race, "Turtle");
        Thread rabbitThread = new Thread(race, "Rabbit");
        turtleThread.start();
        rabbitThread.start();
    }
}
