package expe.seven;

import java.util.HashSet;
import java.util.Set;

class Through implements Runnable {
    @Override
    public void run() {
        synchronized (this) {
            System.out.println(Thread.currentThread().getName() + " 过独木桥");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class Rg8690 {
    public static void main(String[] args) {
        String[] names = { "RX1", "RX2", "RX3", "RX4", "RX5", "RX6", "RX7", "RX8", "RX9", "RX10" };
        Set<Integer> set = new HashSet<Integer>();
        while (set.size() < 10) {
            set.add((int)(Math.random() * 10));
        }
        Through through = new Through();
        for (int i: set) {
            Thread thread = new Thread(through, names[i]);
            thread.start();
        }
    }
}
