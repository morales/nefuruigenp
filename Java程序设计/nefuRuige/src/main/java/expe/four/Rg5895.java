package expe.four;

import java.math.BigInteger;
import java.util.Scanner;

public class Rg5895 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        BigInteger ans = new BigInteger("0");
//        BigInteger bigInteger = new BigInteger(scanner.next());
        int n = scanner.nextInt();
        for(int i=1; i<=n; i++) {
            BigInteger num = new BigInteger("1");
            BigInteger flot = new BigInteger("1");
            for(int j=0; j<i; j++) {
                flot = flot.multiply(num);
                num = num.add(new BigInteger("1"));
            }
            ans = ans.add(flot);
            // System.out.println(i + "! = " + flot);
        }
        System.out.println(ans);
    }
}
