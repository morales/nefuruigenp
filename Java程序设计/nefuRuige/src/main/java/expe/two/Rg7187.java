package expe.two;

import java.util.Scanner;

class Cube {
    int length;
    int width;
    int height;

    void setDemo(int x, int y, int z) {
        length = x;
        width = y;
        height = z;
    }

    int calc() {
        return length*width*height;
    }
}

public class Rg7187 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        int width = scanner.nextInt();
        int height = scanner.nextInt();
        Cube cube = new Cube();
        cube.setDemo(length, width, height);
        System.out.println("体积=" + cube.calc());
    }

}
