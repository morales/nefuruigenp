package expe.two;

import java.util.Scanner;

public class Rg7191 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Market market = new Market();
        market.setMarketName(scanner.next());
        market.addProducts();
        Person person = new Person();
        person.setPersonName("张乐");
        person.shopping(market, new Product(scanner.next()));
        scanner.close();
    }
}

class Product {
    private String productName;

    Product(String name) {
        productName = name;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }
}

class Market {
    private String marketName;
    private Product[] products = new Product[4];

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public Product[] getProducts() {
        return products;
    }

    public void addProducts() {
        products[0] = new Product("电视机");
        products[1] = new Product("洗衣机");
        products[2] = new Product("豆浆机");
        products[3] = new Product("打印机");
    }

    public Product sell(String name) {
        for (Product product: products) {
            // System.out.println("R: " + product.getProductName());
            if(product.getProductName().equals(name)) {
                return product;
            }
        }
        return null;
    }
}

class Person {
    private String personName;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public void shopping(Market market, Product product) {
        if (market.sell(product.getProductName()) == null) {
            System.out.println(personName + "所需商品无货");
        } else {
            System.out.println(personName + "买到了" + product.getProductName());
        }
    }
}