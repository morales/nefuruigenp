package expe.two;

import java.util.Scanner;

class Student {
    private String name;
    private int age;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}

public class Rg7188 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        int age = scanner.nextInt();
        Student student = new Student();
        student.setName(name);
        student.setAge(age);
        System.out.println("该生姓名：" + student.getName() + ",年龄：" + student.getAge());
    }
}
