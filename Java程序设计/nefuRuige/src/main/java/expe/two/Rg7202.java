package expe.two;

import java.util.*;
public class Rg7202{
    public static void main(String[] args) {
        int m,n;    //第一个数的实部和虚部
        int p,q;    //第二个数的实部和虚部
        Scanner scanner =new Scanner(System.in);
        m=Integer.parseInt(scanner.next());
        n=Integer.parseInt(scanner.next());
        p=Integer.parseInt(scanner.next());
        q=Integer.parseInt(scanner.next());
        Complex t =new Complex(m,n);
        Complex s =new Complex(t);
        Complex r=new Complex(p,q);
        (s.Add(r)).Print();
        (t.Sub(r)).Print();
    }
}
// Write your own code
class Complex {
    int am, bm;
    Complex(int a, int b) {
        am = a;
        bm = b;
    }
    Complex(Complex complex) {
        this.am = complex.am;
        this.bm = complex.bm;;
    }
    Complex Add(Complex r) {
        this.am += r.am;
        this.bm += r.bm;
        return this;
    }
    Complex Sub(Complex r) {
        this.am -= r.am;
        this.bm -= r.bm;
        return this;
    }
    void Print() {
        if(am==0 && bm==0) {
            System.out.println("0");
        } else {
            System.out.println(am + " " + bm + "i");
        }
    }
}