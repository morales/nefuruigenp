package expe.two;

import java.util.Scanner;

class Check {
    private int numMax;
    private double multiplyDouble;
    private boolean flag = false;
    Check(int num1, int num2) {
        if (num1 >= num2) {
            numMax = num1;
        } else {
            numMax = num2;
        }
    }
    Check(double num1, double num2, double num3) {
        multiplyDouble = num1 * num2 * num3;
    }
    Check(String str1, String str2) {
        if (str1.equals(str2)) {
            flag = true;
        }
    }
    public int getNumMax() {
        return numMax;
    }

    public double getMultiplyDouble() {
        return multiplyDouble;
    }

    public boolean isFlag() {
        return flag;
    }
}

public class Rg7200 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Check check = new Check(scanner.nextInt(), scanner.nextInt());
        System.out.println("Larger value: " + check.getNumMax());
        check = new Check(scanner.nextDouble(), scanner.nextDouble(), scanner.nextDouble());
        System.out.println("a*b*c = " + check.getMultiplyDouble());
        check = new Check(scanner.next(), scanner.next());
        System.out.println("Are equal: " + check.isFlag());
    }
}
