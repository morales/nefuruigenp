package expe.two;

import java.util.Scanner;

class Prime{
    // Write your own code
    public static boolean isPrime(int num) {
        if (num < 2) {
            return false;
        }
        boolean flag = true;
        for (int i=2; i<num; i++) {
            if (num % i == 0) {
                flag = false;
                break;
            }
        }
        return flag;
    }
}

public class Rg7199 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        // Write your own code
        if (Prime.isPrime(n)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}

