package expe.two;

import java.util.Scanner;

class Point {

    private int x;
    private int y;

    public Point(int xx, int yy)    //构造方法
    {
        this.x = xx;
        this.y = yy;
    }

    public Point(Point point) {
        this.x = point.x;
        this.y = point.y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    //write your code
    public static void fun1(Point p) {
        System.out.println(p.getX());
    }

    public static Point fun2() {
        Point A = new Point(1, 2);
        return A;
    }

}

public class Rg7201 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        Point point = new Point(x, y);
        Point pointTemp = new Point(point);
        System.out.println(pointTemp.getX());
        Point.fun1(pointTemp);
        pointTemp = Point.fun2();
        System.out.println(pointTemp.getX());
    }
}
