package expe.two;

import java.util.Scanner;
class Dog{
    private String name, color;
    private int age;
    Dog() {
        name = "dog1";
        color = "white";
        age = 1;
    }
    Dog(String name, String color, int age) {
        this.name = name;
        this.color = color;
        this.age = age;
    }
    public String getName() {
        return name;
    }
    public String getColor() {
        return color;
    }
    public int getAge() {
        return age;
    }
}

public class Rg7198 {
    public static void main(String[] args) {
        //write code here
        Scanner scanner = new Scanner(System.in);
        Dog d1 = new Dog();
        Dog d2 = new Dog(scanner.next(), scanner.next(), scanner.nextInt());
        System.out.println("name=" + d1.getName() + ",color=" +d1.getColor()+",age="+d1.getAge());
        System.out.println("name=" + d2.getName() + ",color=" +d2.getColor()+",age="+d2.getAge());

    }
}
