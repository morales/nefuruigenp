package expe.one;

import java.util.Scanner;
import java.lang.Math;

public class Rg7194 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int repeat = scanner.nextInt();
        while (repeat != 0) {
            repeat--;
            // Scanner reader = new Scanner(System.in);
            int a = scanner.nextInt();
            int n = scanner.nextInt();
            int ans = a, tmpa = a;
            for (int i=1; i<n; i++) {
                int tmp = (int) Math.pow(10, i);
                a += tmp * tmpa;
                // System.out.println("a=" + a);
                ans += a;
            }
            System.out.println(ans);
        }
    }
}
