package expe.one;

import java.util.Scanner;

public class Rg7196 {
    public static void main(String[] args) {

        int n;

        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();

        //write your code
        double ans = 0;
        for(int i=1; i<=n; i++) {
            if(i%2 == 0) {
                ans -= 1 / ((double)i*3 - 2);
            } else {
                ans += 1 / ((double)i*3 - 2);
            }
        }
        System.out.println(ans);
    }
}
