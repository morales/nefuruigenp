package expe.one;

import java.util.*;

public class Rg7186 {
    public static void main(String[] args) {
        int[] a = {-2,1,4,5,8,12,17,23,45,56,90,100};
        Scanner scanner = new Scanner(System.in);
        boolean flag = false;
        int dex = 0;
        int num = scanner.nextInt();
        for (int i=0; i<a.length; i++) {
            if(a[i] == num) {
                flag = true;
                dex = i;
                break;
            }
        }

        if (flag) {
            System.out.println(num + "是数组中的第" + dex + "个元素");
        }
        else {
            System.out.println(num + "不在数组中");
        }
    }
}
