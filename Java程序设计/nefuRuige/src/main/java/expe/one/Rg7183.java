package expe.one;

import java.util.*;

public class Rg7183 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] num = new int[3];
        int i = 0;
        while (scan.hasNextInt()) {
            num[i] = scan.nextInt();
            i++;
        }
        for (i=0; i<3; i++) {
            for (int j=i+1; j<3; j++) {
                if(num[i] > num[j]) {
                    int tmp = num[i];
                    num[i] = num[j];
                    num[j] = tmp;
                }
            }
        }
        for (i=0; i<3; i++) {
            System.out.print(num[i] + " ");
        }
    }
}
