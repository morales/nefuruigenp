package expe.one;

import java.util.Scanner;

public class Rg7195 {
    public static void main(String[] args) {
        int number, digit1, digit2, digit3, digit4, newnum;
        int temp;
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextInt();
        //WRITE CODE HERE
        digit1 = (number / 1000 + 9) % 10;
        digit2 = (number % 1000 / 100 + 9) % 10;
        digit3 = (number % 100 / 10 + 9) % 10;
        digit4 = (number % 10 + 9) % 10;
        // System.out.println(digit1+ " " + digit2+ " " + digit3+ " " + digit4);
        newnum = digit3 * 1000 + digit4 * 100 + digit1 * 10 + digit2;
        System.out.println("The encrypted number is " + newnum);
    }
}