package expe.one;

import java.util.*;

public class Rg7184 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String lang = scanner.next();
        if (lang.equals("圆形")) {
            double r = scanner.nextDouble();
            System.out.println("圆形面积=" + 3.14 * r * r);
        }
        else if (lang.equals("矩形")) {
            double a = scanner.nextDouble();
            double b = scanner.nextDouble();
            System.out.println("矩形面积=" + a * b);
        }
        else {
            System.out.println("输入的不是圆形或矩形");
        }
    }
}
