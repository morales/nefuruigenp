package expe.three;

interface Paper {
    public String GetName();
}

class Printer {
    public void Print(Paper p) {
        System.out.println(p.GetName());
    }
}

class A4Paper implements Paper {
    @Override
    public String GetName() {
        return "A4";
    }
}

class A6Paper implements Paper {
    @Override
    public String GetName() {
        return "A6";
    }
}

public class Rg7192 {
    public static void main(String[] args) {
        Printer printer = new Printer();
        printer.Print(new A4Paper());
        printer.Print(new A6Paper());
    }
}
