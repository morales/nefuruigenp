package expe.three;

import java.util.Scanner;

class Dog {
    int weight;
    String breed, color;

    Dog(String breed, int weight, String color) {
        this.breed = breed;
        this.weight = weight;
        this.color = color;
    }

    void show() {
        System.out.print("这是一只" + breed + "体重为" + weight + ",颜色为" + color);
    }
}

class UnspottedDog extends Dog {

    UnspottedDog(String breed, int weight, String color) {
        super(breed, weight, color);
    }

    void show() {
        System.out.println("这是一只" + breed + "犬");
    }
}

class SpottedDog extends Dog {
    String spotColor;

    SpottedDog(String breed, int weight, String color, String spotColor) {
        super(breed, weight, color);
        this.spotColor = spotColor;
    }

    void show() {
        super.show();
        System.out.println();
        System.out.println("这是一只" + breed + ",颜色为" + color + ",斑点颜色为" + spotColor);
    }
}

public class Rg7173 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String breed = scanner.next();
        int weight = scanner.nextInt();
        String color = scanner.next();
        String spotColor = scanner.next();
        SpottedDog spottedDog = new SpottedDog(breed, weight, color, spotColor);
        spottedDog.show();
        breed = scanner.next();
        weight = scanner.nextInt();
        color = scanner.next();
        UnspottedDog unspottedDog = new UnspottedDog(breed, weight, color);
        unspottedDog.show();
    }
}
