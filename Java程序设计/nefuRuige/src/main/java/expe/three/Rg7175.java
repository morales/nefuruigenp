package expe.three;

import java.util.Scanner;

interface Employer {
//    String name = null, department = null;
//    double salary = 0;
//
    void showSalary();
    void showBonus();
}

class BasicEmployee implements Employer {
    String name;
    String department;
    double salary;
    public BasicEmployee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }

    @Override
    public void showSalary() {
        System.out.println("我叫" + name + ",在" + department + "部门，我的工资是" + salary);
    }

    @Override
    public void showBonus() {
        System.out.println("我是普通员工，没有奖金，加油升级！");
    }
}

class GoodEmployee implements Employer {
    String name;
    String department;
    double salary, bonus;
    public GoodEmployee(String name, String department, double salary, double bonus) {
        this.name = name;
        this.department = department;
        this.salary = salary;
        this.bonus = bonus;
    }
    @Override
    public void showSalary() {
        System.out.println("我叫" + name + ",在" + department + "部门，我的工资是" + salary);
    }

    @Override
    public void showBonus() {
        System.out.println("我是优秀员工，我的奖金是" + bonus);
    }
}

public class Rg7175 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        BasicEmployee basicEmployee = new BasicEmployee(scanner.next(), scanner.next(), scanner.nextDouble());
        basicEmployee.showSalary();
        basicEmployee.showBonus();
        GoodEmployee goodEmployee = new GoodEmployee(scanner.next(), scanner.next(), scanner.nextDouble(), scanner.nextDouble());
        goodEmployee.showSalary();
        goodEmployee.showBonus();
    }
}
