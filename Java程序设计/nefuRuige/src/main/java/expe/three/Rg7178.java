package expe.three;

import java.util.Arrays;
import java.util.List;

interface USB {
    void turnOn();
    void turnOff();
}

public class Rg7178 {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.add(new MouseRX());
        computer.add(new Keyboard());
        computer.add(new Mic());
        computer.powerOn();
        computer.powerOff();
    }
}

class MouseRX implements USB {
    @Override
    public void turnOn() {
        System.out.println("鼠标启动了");
    }
    @Override
    public void turnOff() {
        System.out.println("鼠标关闭了");
    }
}

class Keyboard implements USB {
    @Override
    public void turnOn() {
        System.out.println("键盘启动了");
    }
    @Override
    public void turnOff() {
        System.out.println("键盘关闭了");
    }
}

class Mic implements USB {
    @Override
    public void turnOn() {
        System.out.println("麦克启动了");
    }
    @Override
    public void turnOff() {
        System.out.println("麦克关闭了");
    }
}

class Computer {
    USB[] usbDevices = new USB[3];

    int num = 0;
    public void add(USB usb) {
        if(num < 3) {
            usbDevices[num++] = usb;
        }
    }

    public void powerOn() {
        for(USB usb: usbDevices) {
            usb.turnOn();
        }
        System.out.println("计算机开机成功");
    }

    public void powerOff() {
        for(USB usb: usbDevices) {
            usb.turnOff();
        }
        System.out.println("计算机关机成功");
    }
}