package expe.three;

import java.util.Scanner;

public class Rg7177 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Cat cat = new Cat(scanner.next());
        cat.speak();
        Mouse mouse = new Mouse(scanner.next());
        mouse.speak();
    }
}

class Mouse extends Animal {
    public Mouse(String name) {
        super(name);
    }

    @Override
    public void speak() {
        System.out.println(name + "的叫声为吱吱");
    }
}

class Cat extends Animal {
    public Cat(String name) {
        super(name);
    }

    @Override
    public void speak() {
        System.out.println(name + "的叫声为喵喵");
    }
}

abstract class Animal {
    String name;
    public Animal(String name) {
        this.name = name;
    }
    public abstract void speak();
}