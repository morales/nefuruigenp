package expe.three;

import java.util.Scanner;

public class Rg7176 {
    public static void main(String[] args) {
        Teacher teacher = new Teacher();
        Scanner scanner = new Scanner(System.in);
        System.out.print("我是英语老师，");
        teacher.printTeach(scanner.next());
        System.out.print("我是数学老师，");
        teacher.printTeach(scanner.next());
    }
}

interface TeacherE {
    void printTeach(String method);
}

class Teacher implements TeacherE {
    @Override
    public void printTeach(String method) {
        System.out.println("I say " + method);
    }
}