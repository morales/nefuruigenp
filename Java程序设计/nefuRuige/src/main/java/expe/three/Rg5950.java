package expe.three;

import java.util.Arrays;
import java.util.Scanner;

interface ComputerAverage {
    //start
    //write your code between start and  end,do not  delete any code
    double average(double[] data);
    //end
}
class Gymnastics implements ComputerAverage {
    public double average(double []data) {
        double sum=0;
        double temp;
        //start
        //write your code between start and  end,do not  delete any code
        Arrays.sort(data);
        for(int i=1; i<data.length-1; i++) {
            sum += data[i];
        }
        return sum / (data.length-2);
        //end
    }
}
class School implements ComputerAverage{
    //start
    //write your code between start and  end,do not  delete any code
    public double average(double[] data) {
        double sum = 0;
        for(double i: data) {
            sum += i;
        }
        sum /= data.length;
    //end
      return sum;
    }
}
public class Rg5950 {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        int n=scan.nextInt();
        double []data=new double[n];
        for(int i=0;i<n;i++){
            data[i]=scan.nextDouble();
        }
        //start
        //write your code between start and  end,do not  delete any code
        ComputerAverage g = new Gymnastics();
        //end
        System.out.print("Gymnastics average is:");
        System.out.printf("%.2f\n",g.average(data));
        //start
        //write your code between start and  end,do not  delete any code
        g = new School();
        //end
        System.out.print("School average is:");
        System.out.printf("%.2f",g.average(data));
    }
}