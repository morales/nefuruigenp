package expe.three;

import java.util.Scanner;

class Vehicle {
    int wheels;
    double weight;
    Vehicle(int wheels, double weight) {
        this.wheels = wheels;
        this.weight = weight;
    }

    void print() {
        System.out.println("汽车:");
    }

    void showInfo() {
        System.out.println("轮子数:" + wheels + "个");
        System.out.println("自身重量:" + weight + "吨");
    }
}

class Car extends Vehicle {
    int passengers;
    Car(int wheels, double weight, int passengers) {
        super(wheels, weight);
        this.passengers = passengers;
    }

    void print() {
        System.out.println("小轿车:");
    }

    void showInfo() {
        super.showInfo();
        System.out.println("额定乘客数:" + passengers + "人");
    }
}

class Truck extends Vehicle {
    int passengers;
    double goods;
    Truck(int wheels, double weight, int passengers, double goods) {
        super(wheels, weight);
        this.passengers = passengers;
        this.goods = goods;
    }

    void print() {
        System.out.println("卡车:");
    }

    void showInfo() {
        super.showInfo();
        System.out.println("额定乘客数" + passengers + "人");
        System.out.println("载重量" + goods + "吨");
    }
}

public class Rg7174 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int wheels = scanner.nextInt();
        double weight = scanner.nextDouble();
        Vehicle vehicle = new Vehicle(wheels, weight);
        vehicle.print();
        vehicle.showInfo();
        wheels = scanner.nextInt();
        weight = scanner.nextDouble();
        int passengers = scanner.nextInt();
        Car car = new Car(wheels, weight, passengers);
        car.print();
        car.showInfo();
        wheels = scanner.nextInt();
        weight = scanner.nextDouble();
        passengers = scanner.nextInt();
        double goods = scanner.nextDouble();
        Truck truck = new Truck(wheels, weight, passengers, goods);
        truck.print();
        truck.showInfo();
    }
}
