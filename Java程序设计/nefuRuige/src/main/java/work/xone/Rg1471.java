package work.xone;

import java.util.Scanner;

class father {
    public String getInfor(String str) {
        System.out.print("this is father ");
        return str;
    }
}

class son extends father {
    public String getInfor(String str) {
        // super.getInfor(str);
        System.out.print("this is son ");
        return str;
    }
}

public class Rg1471 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.next();
        father f = new father();
        System.out.println(f.getInfor(str));
        son s = new son();
        System.out.println(s.getInfor(str));
    }
}
