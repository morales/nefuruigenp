package work.xone;

import java.util.Scanner;
class Vehicle { //汽车类
    //write your own codes
    int wh; // wheels
    int we; // weight
    public Vehicle(int v1, int v2) {
        wh = v1;
        we = v2;
    }
    public void show() {
        System.out.println("car:");
    }

    public void print() {
        System.out.println("Manned number:" + wh + " Bit");
        System.out.println("Capacity:" + we + ".0 Tons");
    }
}

class Car extends Vehicle {//小车类
    //write your own codes
    int pa; // passenger
    Car(int c1, int c2, int c3) {
        super(c1, c2);
        wh = c1;
        we = c2;
        pa = c3;
    }
    public void print() {
        System.out.println("a car:");
        super.print();
        System.out.println("Manned number:" + pa + " Bit");
    }

}

class Truck extends Vehicle {//卡车类
    //write your own codes
    int pa, pay;
    Truck(int t1, int t2, int t3, int t4) {
        super(t1, t2);
        wh = t1;
        we = t2;
        pa = t3;
        pay = t4;
    }
    public void print() {
        System.out.println("truck:");
        super.print();
        System.out.println("Manned number:" + pa + " Bit");
        System.out.println("Capacity:" + pay + ".0 Tons");
    }

}

public class Rg1363 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int v1 = scan.nextInt();
        int v2 = scan.nextInt();

        int c1 = scan.nextInt();
        int c2 = scan.nextInt();
        int c3 = scan.nextInt();

        int t1 = scan.nextInt();
        int t2 = scan.nextInt();
        int t3 = scan.nextInt();
        int t4 = scan.nextInt();

        Vehicle ve1 = new Vehicle(v1, v2);
        ve1.show();
        ve1.print();
        Car car1 = new Car(c1, c2, c3);
        car1.print();
        Truck tr1 = new Truck(t1, t2, t3, t4);
        tr1.print();
    }

}